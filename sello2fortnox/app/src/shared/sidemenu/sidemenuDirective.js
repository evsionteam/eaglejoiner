var sideMenu = function( DataService, SF, Utils, helper ){
	return {
		restrict : 'E',
		replace  : true,
		templateUrl : SF.sharedPath + '/sidemenu/sidemenuView.html',
		link : function( scope, ele, atts ){

		},
		controller : function( $rootScope, $scope ){
                   
                    
			if( ! helper.isExist( $rootScope.gscope.menu ) ){
				DataService.getMenu().then(function( response ){
					$rootScope.gscope.menu = response;
				},function( error ){
					Utils.log([error, 'Could not fetch menu'], 'error' );
				});
			}
			
		}
	}
}
app.directive('sideMenu', sideMenu );