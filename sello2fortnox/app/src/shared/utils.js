var Utils = function( $rootScope, $cookies, $location, $log, SF, helper, DataService, Base64 ){

	var utils = {};
	
	utils.pop = function (data, ID) {
        return  data.filter(function (el) {
            return el.ID != ID
        });
    }

	utils.log = function( data, key ){

		if( ! helper.isExist( data ) || ! SF.scriptDebug ){
			return false;
		}

		switch( key ){
			case 'error':
				$log.error( data );
			break;

			case 'info':
				$log.info( data );
			break;

			case 'warning':
				$log.warn( data );
			break;

			default :
				$log.log( data );
			break;
		}
	}

	utils.getYears = function(){
		var date = new Date();
		var data = [];
		for( var i=0; i<20; i++ ){
			data.push( date.getFullYear() + i );
		}

		return data;
	}

	utils.setGlobalUserData = function( data ){
		var cookie = utils.getCookies();

		if( !helper.isExist( cookie ) ){
			utils.log( 'Cannot set Global Data.', 'error' );
			return;
		}

		$rootScope.gscope.user = {
						id                    	   : cookie.id,
						name                  	   : cookie.username,
						email                 	   : cookie.email,
						userUniqueUrl         	   : cookie.user_unique_url,
						connectedToFrotnox    	   : cookie.connected_to_frotnox,
						fortnoxClientSecret        : helper.isExist( cookie.fortnox_client_secret ) ? Base64.decode( cookie.fortnox_client_secret ) : cookie.fortnox_client_secret,
						fortnoxAuthorizationToken  : helper.isExist( cookie.fortnox_authorization_token ) ? Base64.decode( cookie.fortnox_authorization_token ) : cookie.fortnox_authorization_token,
						stripeCustomer             : cookie.stripe_customer,
						firstName				   : cookie.first_name,
						lastName				   : cookie.last_name,
						phone					   : cookie.phone,
						gender					   : cookie.gender,
						address					   : cookie.address,
						profilePic				   : cookie.user_avatar,
						fortnoxProcess			   : cookie.fortnox_process,
						paid					   : cookie.paid						
					}
	}

	utils.toast = function( msg , type ){

        toastr.options.showMethod = 'slideDown';
        toastr.options.hideMethod = 'fadeOut';
        toastr.options.closeButton = true;
        toastr.options.timeOut = 15000;
        var time = 7000;

        if( angular.isUndefined( type ) ){
            toastr.info( msg );
        }else if( type == 'warning' ){
            toastr.warning(msg  );
        }else if( type == 'error' ){
            toastr.error(msg );
        }else if( type == 'success' ){
            toastr.success(msg);
        }
    }

    utils.removeToast = function(){
    	toastr.remove();
    }

	utils.redirect = function( key ){

		if( ! helper.isExist( key ) ){

			utils.log( 'Redirect Key is undefined.', 'warning' );

			return false;
		}
		switch( key ){
			case 'login' :
				$location.path( '/login' );
			break;

			case 'dashboard' :
				$location.path( '/' );
			break;

			case 'log' :
				$location.path( '/log' );
			break;

			case 'setting' :
				$location.path( '/setting' );
			break;

			default :

			break;
		}
	}

	utils.removeCookies = function () {
        $cookies.remove( SF.cookieVar );
    }
    
	utils.setCookies = function (data) {
		utils.removeCookies();
        $cookies.putObject( SF.cookieVar , JSON.stringify ( data ) );
    }

    utils.logout = function(){
    	utils.removeCookies();
    	utils.redirect( 'login' );
    	DataService.user().logout();
    }

    utils.getCookies = function () {

    	var cookie = $cookies.getObject( SF.cookieVar );

    	if( helper.isExist( cookie ) )
        	return JSON.parse( cookie );
        else{
        	utils.log( 'Cookie not found.', 'info' );
        }
    }

	return utils;
}

app.factory( 'Utils', Utils );