var topHeader = function (SF, Utils,$window) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: SF.sharedPath + '/topheader/topHeaderView.html',
        controller: function ($scope) {

            var windowWidth = $(window).width(),
                $menu = $('.menu-icon');
        
                if (windowWidth < 767) {
                    $menu.trigger('click');
                }

            $scope.logout = function () {

                Utils.logout();
            }
            
            
            
        }
    }
}
app.directive('topHeader', topHeader);