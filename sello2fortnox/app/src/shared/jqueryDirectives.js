var myDatepicker = function ($parse) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, ele, attrs) {
            jQuery(ele).datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: function (date) {

                    var ngModel = $parse(attrs.ngModel);

                    scope.$apply(function (scope) {
                        // Change binded variable
                        ngModel.assign(scope, date);
                    });
                }
            });
        }
    }
}
app.directive('myDatepicker', myDatepicker);

var selectmenu = function ($parse, $timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, ele, attrs) {

            $timeout( function(){
                jQuery(ele).selectmenu({
                change: function (event, ui) {

                    var ngModel = $parse(attrs.ngModel);

                    scope.$apply(function (scope) {
                        ngModel.assign(scope, ui.item.value);
                    });
                }
            });
            })
            
        }
    }
}
app.directive('selectmenu', selectmenu);

var dynamicHeight = function () {
    return {
        restrict: 'A',
        link: function (scope, ele, atts) {
            // jQuery(ele).selectmen  ();

            var dynamicHeight = function () {
                var $background   = $('.background '),
                    elementHeight = $(ele).outerHeight(),
                    windowHeight  = $(window).outerHeight(),
                    windowWidth   = $(window).width();
                
                // dynamic width 
                $background.css('width', windowWidth + 'px');
                
                // daynamic height 
                if (windowHeight < elementHeight) {
                    var wrapperHeight = (50 + elementHeight);
                    $background.css('height', wrapperHeight + 'px');
                } else {
                    $background.height(windowHeight);
                }
                
            }
            setTimeout(function () {
                dynamicHeight();
                $(window).resize(dynamicHeight);
            }, 1000);


        }
    }
}
app.directive('dynamicHeight', dynamicHeight);



var pageNotFound = function () {
    return {
        restrict: 'A',
        link: function (scope, ele, atts) {
            // jQuery(ele).selectmen  ();
            

        }
    }
}
app.directive('pageNotFound', pageNotFound);
