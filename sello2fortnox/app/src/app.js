var app = angular.module( 'sello2frotnox', [ 'ngSanitize','angularPayments','checklist-model', 'ui.bootstrap', 'ngRoute', 'ngResource', 'ngCookies', 'angularUtils.directives.dirPagination','angular-ladda', 'ngHelper', 'oitozero.ngSweetAlert', 'ngImgCrop', 'ngCsv' ] );
var apiPath = "//"+ window.location.host + "/eaglejoiner/sello2fortnox/api/index.php";
var host = '//' + window.location.host + '/eaglejoiner';

if( window.location.host.match(/eaglejoiner/g) ){
    apiPath = "//"+ window.location.host + "/api/index.php";
    host = '//' + window.location.host;
}

app.constant( 'SF', {
		apiPath       :  apiPath,
		scriptDebug   : false,
		srcPath       : 'app/src',
		componentPath : 'app/src/components',
		sharedPath    : 'app/src/shared',
		cookieVar     : 'sello2frotnox',
		uploadsPath   : 'api/',
		host          :  host
	});

app.config( function( $routeProvider, SF, laddaProvider ){ 

	laddaProvider.setOption({ /* optional */
      style: 'zoom-in',
      spinnerSize: 30,
      spinnerColor: '#ffffff'
    });

    var isTokenValid = function( DataService, $q, Utils ){

		var defer = $q.defer();

		var onToken = function( response ){

			if( response.status == 200 ){
				
				defer.resolve( response );
			}else{
				Utils.log( 'Invalid Token', response );
				Utils.removeCookies();
				Utils.redirect( 'login' );
			}
		}

		var onTokenError = function( error ){
			Utils.log( 'Invalid Token', error );
			Utils.logout();
		}

		DataService.user().isValid( onToken , onTokenError );

		return defer.promise;
	}
    
	$routeProvider
		.when( '/', {
			templateUrl : SF.componentPath + '/dashboard/dashboardView.html',
			controller  : 'dashboardCtrl',
			resolve     : {
				logs : function( DataService, $q, Utils ){

					var defer = $q.defer();

					var onLog = function( response ){
						
						defer.resolve( response.data );
					}

					var onError = function( error ){
						Utils.log( [ 'Error on Getting log', error], 'error' );
						Utils.logout(); 
					}

					DataService.log().get( {pagenum :1 }, onLog, onError );

					return defer.promise;
				}
			}
		})
		.when( '/login',{
			templateUrl : SF.componentPath + '/login/loginView.html',
			controller  : 'loginCtrl'
		})
		.when( '/register',{
			templateUrl : SF.componentPath + '/register/registerView.html',
			controller  : 'registerCtrl',
		})
		.when( '/recover',{
			templateUrl : SF.componentPath + '/recover/recoverView.html',
			controller  : 'recoverCtrl',
		})
		.when( '/setting',{
			templateUrl : SF.componentPath + '/setting/settingView.html',
			controller  : 'settingCtrl',
			resolve     : {
				isTokenValid : isTokenValid
			}
		})
		.when( '/log',{
			templateUrl : SF.componentPath + '/log/logView.html',
			controller  : 'logCtrl',
			resolve     : {
				isTokenValid : isTokenValid
			}
		})
		.when( '/profile',{
			templateUrl : SF.componentPath + '/editprofile/editProfileView.html',
			controller  : 'editProfileCtrl',
			resolve     : {
				isTokenValid : isTokenValid
			}
		})
		.otherwise({
			templateUrl : SF.componentPath + '/pagenotfound/pagenotfoundView.html',
			controller  : 'pagenotfoundCtrl',
		})
});

app.run( function( $rootScope, DataService, Utils, helper, $timeout, Base64 ){

    DataService.getConfig().then(function( response ){
		Stripe.setPublishableKey( response.stripePublishableKey );
	});

    $rootScope.gscope = {
    	pageTitle : 'sello2frotnox',
    	user      : null
    } 

    $rootScope.$on( '$routeChangeSuccess',function( event, next, current ){
    	$timeout(function(){
    		Utils.removeToast();
    	},2000);
    });

	$rootScope.$on( "$routeChangeStart", function( event, next, current ){

		var cookie = Utils.getCookies(),
		    slug = helper.getUriSegment(1);
		if( helper.isExist( cookie ) ){

			//setting auth token for dataservice
			DataService.headers.authToken = Base64.decode( cookie.auth_token );
			
			if( slug == 'login' ){
				Utils.redirect( 'dashboard' );
			}else{

				$rootScope.gscope.slug = slug;

				if( ! helper.isExist( $rootScope.gscope.user ) ){
					Utils.setGlobalUserData();
				}
			}

		}else{

			var exception = [ 'register', 'recover' ];
			
			if( exception.indexOf( slug  ) == -1 )
				Utils.redirect( 'login' );
		}
        
    });
} )
