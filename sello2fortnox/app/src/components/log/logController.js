var logCtrl = function( $rootScope, $scope, DataService, Utils, helper, $timeout, $parse ){
	
	$rootScope.gscope.pageTitle   = 'Activity Log';
	$rootScope.gscope.pageHeading = 'Activity Log';

	$scope.paginate = {
		currentPage  : 1,
		itemsPerPage : 15,
		totalRows    : 10
	}

	$scope.uiState = {
		checked : false,
		checkedLogs : [],
		isGettingLog : false,
		isFiltering  : false
	}

	$scope.logFilter = {
		response_code  : '',
		activity_type  : '',
		gt_date        : '',
		lt_date        : '',
	}

	$scope.log = [];

	$scope.responseCode = [ 200, 201, 400, 401, 404, 500 ];
	$scope.activityType = [ 'orderUpdate','orderNew', 'itemCreate', 'priceCreate', 'orderCancel', 'creditInvoice', 'newInvoice' ];

	function getLogs(){
		$scope.paginate.currentPage = 1;
		$scope.getLogs();
	}

	$scope.refresh = function(){
		getLogs();
	}

	$scope.getLogs = function( page ){ 

		$scope.uiState.isGettingLog = true;

		if( typeof page == 'undefined' ){
			page = $scope.paginate.currentPage;
		}else{
			$scope.paginate.currentPage = page;
		}

		var onLog = function( response ){

			if( response.status == 200 ){

				if( helper.isExist(response.filter.response_code) ){
					$scope.responseCode = response.filter.response_code; 
				}

				if( helper.isExist(response.filter.activity_type) ){
					$scope.activityType = response.filter.activity_type; 
				}

				$scope.log                 = response.data;
				$scope.paginate.totalRows  = response.total_rows;
				$scope.uiState.checkedLogs = [];
			}

			$timeout(function(){
				$scope.uiState.isGettingLog = false;
			},1000);
		}

		var onLogError = function( error ){
			Utils.log( [ 'Could not get activity Log', error ], 'error' );
			$scope.uiState.isGettingLog = false;
		}

		var filteration = {
			filter : {

			}
		}

		var needFilteration = false;

		var page = page || 1;

		var logFilter = angular.copy( $scope.logFilter );

		for( var lf in logFilter ){
			
			if( logFilter[lf] !== '' ){
				needFilteration = true;
				filteration.filter[lf] = logFilter[lf];
			}
		}

		if( needFilteration ){
			DataService.log().filter( { param2: page }, filteration, onLog, onLogError );
		}else{
			DataService.log().get( { param1: page }, onLog, onLogError );
		}
	}

	$scope.$watch( 'logFilter', function(){
		
		var isFiltering = false;
		angular.forEach( $scope.logFilter, function( value ){
			isFiltering = value != '' ? true : isFiltering;
		});

		$scope.uiState.isFiltering = isFiltering;

		getLogs();
	},true);
	
	$scope.clearDateFilter = function(){
		$scope.logFilter.gt_date = '';
		$scope.logFilter.lt_date = '';
	}

	$scope.export = function( $event ){

		$event.stopPropagation();
		var ele = document.getElementById('export');

		var onGet = function( response ){

			if( response.status == 200 ){
				$scope.allLog = response.data;
				$timeout( function(){
					angular.element(ele).triggerHandler('click');
				},0)
			}
		}

		var onGetError = function( error ){
			Utils.log( ['Error on getting Log',error], 'error' );
			Utils.toast( 'Error Occured' );
		}

		swal({
		   title: "Are you sure to export all activity?",
		   type: "warning",
		   showCancelButton: true,
		   confirmButtonColor: "#DD6B55",
		   confirmButtonText: "Export it!",
		   closeOnConfirm: true}, 
		function(){ 
			DataService.log().getAll( onGet, onGetError );
		});
	}

	$scope.stateToggle = function( handler ){

		if( helper.isExist( handler ) )
			$scope.uiState[ handler ] = !$scope.uiState[ handler ];
	}
	
	$scope.checkAll = function(){

		if( $scope.uiState.checked ){
			$scope.stateToggle( 'checked' );
			$scope.uiState.checkedLogs = [];

		}else{

			$scope.uiState.checkedLogs = $scope.log.map(function (log) {
	            return log.id;
	        });

			$scope.stateToggle( 'checked' );
		}
	}

	$scope.deleteChecked = function(){

		var onDelete = function( response ){

			if( response.status == 200 ){
				$scope.uiState.checkedLogs = [];
	        	$scope.log = [];
	        	Utils.toast( 'Activity deleted successfully.', 'success' );
			}else{
				Utils.toast( response.message, 'error' );
			}
		}

		var onDeleteError = function( error ){
			Utils.toast( 'Cannot Delete. Try again later.', 'error' );
			Utils.log( [ 'Cannot Delete. Try again later.', error ] , 'error' );
		}

		// if( $scope.uiState.checkedLogs.length == 0 ){
		// 	Utils.toast( 'Please Select Item to delete.', 'warning' );
		// 	return;
		// }

		swal({
		   title: "Are you sure?",
		   text: "Your will not be able to recover this activity!",
		   type: "warning",
		   showCancelButton: true,
		   confirmButtonColor: "#DD6B55",
		   confirmButtonText: "Yes, delete it!",
		   closeOnConfirm: true}, 
		function(){ 
			DataService.log().deleteAll( onDelete, onDeleteError );
		});
	}
}

app.controller( 'logCtrl', logCtrl );