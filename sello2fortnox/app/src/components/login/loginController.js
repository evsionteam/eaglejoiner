var loginCtrl = function( $rootScope, $scope, DataService, helper, Utils, SF, $location, Base64 ){
	
	$rootScope.gscope.pageTitle = 'Login';

	$scope.uiState = {
		isLoggingIn : false
	};

	$scope.logo = SF.host + '/sello2fortnox/assets/img/logo.png';
	
	$scope.doLogin = function(){
		
		$scope.uiState.isLoggingIn = true;

		var onLogin = function(data){

			var response = data.data;

			if( ! helper.isExist( response ) ){
				Utils.toast( 'Oops, Something went wrong. Please try again.', 'warning' );
				Utils.log( [ 'No response from server', data ], 'error' );
				$scope.uiState.isLoggingIn = false;

				return;
			}

			DataService.headers.authToken = response.authToken;
			Utils.log( [ 'on Login', response ] );
			var cookieData = {
				auth_token 		      			: Base64.encode( response.auth_token ),
				id		   		      			: response.id,
				username   		      			: response.username,
				email      		      			: response.email,
				user_unique_url 	  			: $location.protocol() + ':' + response.user_unique_url,
				fortnox_client_secret 			: helper.isExist( response.fortnox_client_secret ) ? Base64.encode( response.fortnox_client_secret ) : response.fortnox_client_secret,
				fortnox_authorization_token     : helper.isExist( response.fortnox_authorization_token ) ? Base64.encode( response.fortnox_authorization_token ) : response.fortnox_authorization_token,
				stripe_customer					: helper.isExist(response.stripe) ? response.stripe.stripe_id : null,
				fortnox_process					: typeof response.fortnox_process == 'undefined' ? 0 : response.fortnox_process,
				first_name						: response.first_name,
				last_name						: response.last_name,
				phone							: response.phone,
				gender							: response.gender,
				address							: response.address,
				paid 							: helper.isExist(response.stripe) ? response.stripe.paid : 0,
				user_avatar						: helper.isExist( response.user_avatar ) ? SF.uploadsPath + response.user_avatar : undefined,
				connected_to_frotnox            : helper.isExist( response.fortnox_token ) ? true : false
			}
			Utils.log( cookieData );
			
			Utils.setCookies( cookieData );
			Utils.setGlobalUserData();
			Utils.redirect( 'dashboard' );
		}

		var onLoginError = function( error ){

			$scope.uiState.isLoggingIn = false;
			
			Utils.log( [error,'Login Error'],'error' );
			if( helper.isExist( error.data.field ) ){
				angular.forEach( error.data.field, function( value, key ){

					switch( key ){
						case 'password':
							$scope.loginForm.password.$setValidity("required", false);

						break;

						case 'username':
							$scope.loginForm.username.$setValidity("required", false);
						break;
					}

					Utils.toast( value, 'error' );
				});
			}
		}
		
		DataService.login( $scope.login ).then( onLogin, onLoginError ) ;
	}

}

app.controller( 'loginCtrl', loginCtrl );
