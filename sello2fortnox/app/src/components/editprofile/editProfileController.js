var editProfileCtrl = function ($scope, $rootScope, $uibModal, Utils, DataService, helper, SF, Base64) {

    $rootScope.gscope.pageTitle = 'Edit Profile';
    $rootScope.gscope.pageHeading = 'Profile';

    $scope.uiState = {
        isSaving: false,
        isChangingPassword: false,
        isProcessingCard: true,
        isFetchingSubscription: true,
        showPasswordBox      : true
    }
    
    $scope.togglePassword = function(){
        $('.password-box').slideToggle();
        $scope.uiState.showPasswordBox = !$scope.uiState.showPasswordBox;
    }

    if ($rootScope.gscope.user.stripeCustomer) {
        var onGetCard = function (response) {

            $scope.cards = response.data.data;
            $scope.uiState.isProcessingCard = false;
        }

        var onGetCardError = function (error) {

            $scope.uiState.isProcessingCard = false;
            Utils.toast('Could not fetch cards', 'error');
            Utils.log(['error on getting card', error], 'error');
        }
        DataService.card().get({id: $rootScope.gscope.user.stripeCustomer}, onGetCard, onGetCardError);
    } else {
        $scope.uiState.isProcessingCard = false;
    }

    $rootScope.$on("getProfileImage", function (event, data) {
        $scope.croppedAvatar = data.croppedProfilePic;
        $scope.originalAvatar = data.originalPic;
    });

    $scope.editedProfile = {
        first_name: $rootScope.gscope.user.firstName,
        last_name: $rootScope.gscope.user.lastName,
        address: $rootScope.gscope.user.address,
        phone: parseInt($rootScope.gscope.user.phone),
        gender: $rootScope.gscope.user.gender
    }

    $scope.subscription = [];

    $rootScope.$on('cardUpdate', function (event, updatedCard) {
        angular.forEach($scope.cards, function (value, key) {
            if (updatedCard.id == value.id) {
                $scope.cards[key] = updatedCard;
            }
        });
    });

    $rootScope.$on("addCard", function (event, newCard) {
        $scope.cards.push(newCard);
    });

    $rootScope.$on('processingCard', function (event, status) {

        if (status) {
            $scope.uiState.isProcessingCard = true;
        } else {
            $scope.uiState.isProcessingCard = false;
        }
    });

    $scope.changePassword = function (form) {

        $scope.uiState.isChangingPassword = true;

        var onChange = function (response) {

            if (response.status == 200) {
                // $scope.pass = {};

                // form.old_password.$setValidity( "required",true );
                // form.new_password.$setValidity( "required",true );

                Utils.toast("Password changed successfully.", 'success');
                var cookie = Utils.getCookies();

                DataService.headers.authToken = response.data.new_token;
                cookie.auth_token = Base64.encode(response.data.new_token);
                Utils.setCookies(cookie);

            } else {
                Utils.toast(response.message, 'error');
            }
            Utils.log(response);
            $scope.uiState.isChangingPassword = false;
        }

        var onChangeError = function (error) {

            $scope.uiState.isChangingPassword = false;
            Utils.log(['Error on Changing Password', error], 'error');
            Utils.toast('Error on Changing Password.', 'error');
        }

        DataService.user().changePassword($scope.pass, onChange, onChangeError);
    }

    $scope.editProfilePic = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'editProfilePic.html',
            controller: function ($scope, $rootScope, $uibModalInstance) {
                $scope.profilePic = '';
                $scope.croppedProfilePic = '';
                var handleFileSelect = function (evt) {
                    var file = evt.currentTarget.files[0];
                    var reader = new FileReader();
                    reader.onload = function (evt) {
                        $scope.$apply(function ($scope) {
                            $scope.profilePic = evt.target.result;
                        });
                    };
                    reader.readAsDataURL(file);
                    $scope.cropArea = true;
                };
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    if ($scope.profilePic !== '') {
                        $rootScope.$emit("getProfileImage", {"croppedProfilePic": $scope.croppedProfilePic, "originalPic": $scope.profilePic});
                    }
                }
                jQuery(document).on('change', '#xtremeFileInput', handleFileSelect)
            },
            size: 'md'
        });
    }

    $scope.save = function () {
        $scope.uiState.isSaving = true;

        var onSave = function (response) {

            if (response.status == 200) {

                Utils.toast('Your profile has been updated.', 'success');
                var cookie = Utils.getCookies();
                angular.forEach($scope.editedProfile, function (value, key) {
                    cookie[key] = value;
                });

                if ($scope.croppedAvatar) {
                    cookie.user_avatar = SF.uploadsPath + response.data.user_avatar;
                }

                Utils.setCookies(cookie);
                Utils.setGlobalUserData();
            }

            $scope.uiState.isSaving = false;


            Utils.log(response);

        }

        var onSaveError = function (error) {
            $scope.uiState.isSaving = false;
            Utils.toast('Error on Updating profile', 'error');
            Utils.log(['Error on Addind Profile', error], 'error');
        }

        var update = {
            meta: $scope.editedProfile
        }

        if ($scope.croppedAvatar) {
            update.meta.user_avatar = $scope.croppedAvatar;
        }

        DataService.user().update(update, onSave, onSaveError);
    }

    $scope.editCard = function (card) {

        $uibModal.open({
            animation: true,
            templateUrl: 'editCard.html',
            controller: function ($scope, $rootScope, $uibModalInstance, DataService, Utils, card) {

                $scope.uiState = {
                    isUpdating: false
                }

                $scope.month = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

                $scope.years = Utils.getYears();

                $scope.card = {
                    exp_month: card.exp_month,
                    exp_year: card.exp_year,
                    name: card.name,
                    address_line1: card.address_line1,
                    address_line2: card.address_line2,
                    address_city: card.address_city,
                    address_zip: card.address_zip,
                    address_state: card.address_state,
                    address_country: card.country,
                }

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }

                $scope.update = function () {

                    $rootScope.$emit('processingCard', true);

                    $scope.uiState.isUpdating = true;
                    var onUpdate = function (response) {
                        $rootScope.$emit('processingCard', false);
                        if (response.status == 200) {
                            $rootScope.$emit('cardUpdate', response.data);
                            Utils.toast(response.message, 'success');
                        } else {
                            Utils.toast(response.message, 'error');
                        }

                        $scope.uiState.isUpdating = false;
                        $scope.cancel();
                    }

                    var onUpdateError = function (error) {
                        $scope.uiState.isUpdating = false;
                        $rootScope.$emit('processingCard', false);
                        $scope.cancel();
                        Utils.toast('Error on Card Update', 'error');
                        Utils.log(['Error on card update', error], 'error');
                    }

                    var data = {
                        stripeCustomer: card.customer,
                        stripeCard: card.id,
                        card_data: $scope.card
                    }

                    DataService.card().update(data, onUpdate, onUpdateError);
                }
            },
            resolve: {
                card: card
            }
        });
    }

    $scope.addCard = function () {

        if (helper.isExist($rootScope.gscope.user.stripeCustomer)) {
            $uibModal.open({
                animation: true,
                templateUrl: 'addCard.html',
                controller: function ($scope, $rootScope, $uibModalInstance, DataService, Utils) {

                    $scope.month = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

                    $scope.years = Utils.getYears();

                    $scope.card = {
                        exp_year: new Date().getFullYear() + 1,
                        exp_month: 1
                    }

                    $scope.uiState = {
                        isAdding: false
                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    }

                    $scope.save = function () {

                        $scope.uiState.isAdding = true;

                        var onSave = function (response) {
                            $rootScope.$emit('processingCard', false);

                            if (response.status == 200) {
                                $rootScope.$emit('addCard', response.data);
                                Utils.toast(response.message, 'success');
                            } else {
                                Utils.toast(response.message, 'error');
                            }

                            $scope.uiState.isAdding = false;
                            $scope.cancel();
                        }

                        var onSaveError = function (error) {
                            $rootScope.$emit('processingCard', false);
                            $scope.uiState.isAdding = false;
                            Utils.toast('Something went wrong', 'error');
                            Utils.log(['Error on Add card', error], 'error');
                        }

                        var data = {
                            stripeCustomer: $rootScope.gscope.user.stripeCustomer,
                            card_data: $scope.card
                        }

                        $rootScope.$emit('processingCard', true);
                        DataService.card().save(data, onSave, onSaveError);
                    }
                }
            });
        } else {
            Utils.toast('Please be Stripe customer to add card.', 'warning');
        }
    }

    $scope.deleteCard = function (c) {

        var onDelete = function (response) {
            $scope.uiState.isProcessingCard = false;
            if (response.status == 200) {

                angular.forEach($scope.cards, function (value, key) {

                    if (value.id == c.id) {
                        $scope.cards.splice(key, 1);
                    }
                });

                Utils.toast(response.message, 'success');
            } else {
                Utils.toast(response.message, 'error');
            }
        }

        var onDeleteError = function (error) {
            $scope.uiState.isProcessingCard = false;
            Utils.toast('Something went wrong.', 'error');
            Utils.log([error, 'error on delete card'], 'error');
        }

        swal({
            title: "Are you sure?",
            text: "Your will not be able to recover this activity!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true},
                function () {
                    $scope.uiState.isProcessingCard = true;
                    DataService.card().delete({id: c.customer, id2: c.id}, onDelete, onDeleteError);
                });
    }

    $scope.unsubscribe = function (subs) {

        var onCancel = function (response) {

            $scope.uiState[ 'isCancellingSubscription_' + subs.id ] = false;

            if (response.status == 200) {

                angular.forEach($scope.subscription, function (value, key) {
                    if (value.id == subs.id) {
                        value.cancel_at_period_end = true;
                    }
                });

                Utils.toast(response.message, 'success');

            } else {
                Utils.toast(response.message, 'error');
            }
        }

        var onCancelError = function (error) {

            $scope.uiState[ 'isCancellingSubscription_' + subs.id ] = false;

            Utils.toast('Error on unsubscribe.', 'error');
            Utils.log([error, 'Error on unsubscribe.'], 'error');
        }

        swal({
            title: "Are you sure to unsubscribe?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            closeOnConfirm: true},
                function () {

                    $scope.$apply(function () {
                        $scope.uiState[ 'isCancellingSubscription_' + subs.id ] = true;
                    });

                    DataService.subscription().cancel({id: subs.customer}, onCancel, onCancelError);
                });
    }



    if ($rootScope.gscope.user.stripeCustomer) {
        var onSubscription = function (response) {
            Utils.log(response);
            $scope.uiState.isFetchingSubscription = false;
            $scope.subscription = response.data.data;
        }

        var onSubscriptionError = function (error) {
            $scope.uiState.isFetchingSubscription = false;
            Utils.toast('Something went wrong.', 'error');
            Utils.log([error, 'error on getting subscription'], 'error');
        }

        DataService.subscription().query(onSubscription, onSubscriptionError);
    } else {
        $scope.uiState.isFetchingSubscription = false;
    }

    $scope.updateSubscription = function (subs) {

        var onUpdate = function (response) {
            $scope.uiState[ 'isCancellingSubscription_' + subs.id ] = false;

            if (response.status == 200) {

                angular.forEach($scope.subscription, function (value, key) {

                    if (value.id == subs.id) {
                        value.cancel_at_period_end = false;
                    }
                });

                Utils.toast(response.message, 'success');

            } else {
                Utils.toast(response.message, 'error');
            }
        }

        var onUpdateError = function (error) {
            $scope.uiState[ 'isCancellingSubscription_' + subs.id ] = false;

            Utils.toast('Error on update subscription.', 'error');
            Utils.log([error, 'Error on update subscription.'], 'error');
        }

        swal({
            title: "Are you sure to update your subscription?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            closeOnConfirm: true},
                function () {

                    $scope.$apply(function () {
                        $scope.uiState[ 'isCancellingSubscription_' + subs.id ] = true;
                    })

                    DataService.subscription().update({stripeSubscriptionId: subs.id}, onUpdate, onUpdateError);

                });
    }
}

app.controller('editProfileCtrl', editProfileCtrl);
