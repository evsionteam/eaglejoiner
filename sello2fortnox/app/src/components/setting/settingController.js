var settingCtrl = function( $rootScope,$uibModal, $scope, DataService, Utils, helper, $interval, $timeout, Base64 ){

	var heading = "Subscribe";

	if( $rootScope.gscope.user.stripeCustomer ){ 
		heading = 'Setting';
	}else{
		$scope.showWelcomeText = true;
	}
	
	$rootScope.gscope.pageTitle = heading; 
	$rootScope.gscope.pageHeading = heading;

	$scope.uiState = {
		isGettingAuthToken : false,
		isSubscribing	   : false
	}

	$scope.selloUrl = null;
	$scope.popOverFortnoxTemplateUrl = 'fortnoxInfo.html';
	$scope.popOverSelloTemplateUrl   = 'selloInfo.html';
	$scope.showWelcomeText = false;
	var watchFortnoxProcessInstance;
    $scope.notes = [];
    var fortnoxProcess = parseInt($rootScope.gscope.user.fortnoxProcess);
        fortnoxProcess = ( fortnoxProcess == 0 || fortnoxProcess == 1  ) ? fortnoxProcess : undefined;
	
	if( $rootScope.gscope.user.fortnoxAuthorizationToken && fortnoxProcess == 0 ){
		showSelloUrl();
	}
	if( parseInt( $rootScope.gscope.user.paid ) == 1 && fortnoxProcess == 1 ){
		fortnoxProcessingNotification();
	}

	if( $rootScope.gscope.user.fortnoxAuthorizationToken && fortnoxProcess == 1 ){

		if( angular.isUndefined( watchFortnoxProcessInstance ) ){
			watchFortnoxProcess();
		}
	}
	
	function fortnoxProcessingNotification(){
		$scope.notes = [{
	            message:"Since you still have one more steps to complete, you won't be able to give you full fledged services. To unlock complete feature of sello 2 fortnox, please fill up the form below:",
	            showBtn: false,
	            class : 'note-info'
	    }];
		
	}

	function showSelloUrl(){
		$scope.selloUrl = angular.copy($rootScope.gscope.user.userUniqueUrl);
	}

	if( $rootScope.gscope.user.connectedToFrotnox ){
		$scope.fortnox = {
			client_secret      : $rootScope.gscope.user.fortnoxClientSecret,
			authorization_code : $rootScope.gscope.user.fortnoxAuthorizationToken
		}
	}
	
	$scope.saveToFortnox = function( $inputData, $form ){
		var onGet = function( response ){
			
			//Utils.toast( response.message ,'success' );

			var cookieData = Utils.getCookies();

			cookieData.connected_to_frotnox = true;
			cookieData.fortnox_client_secret = Base64.encode( $inputData.client_secret );
			cookieData.fortnox_authorization_token = Base64.encode( $inputData.authorization_code );
			cookieData.fortnox_process = 1;
			Utils.setCookies( cookieData );
			Utils.setGlobalUserData();

			//Fetching fortnox account data
			var onFetch = function( response ){
				$scope.uiState.isGettingAuthToken = false;

				Utils.toast( response.data.message, 'success' );
			}

			var onFetchError = function( error ){

				$scope.uiState.isGettingAuthToken = false;

				Utils.toast( 'Error on getting fortnox data. Please Try again!', 'error' );

				Utils.log( [ error,'Fetching Frotnox Account data error.' ],'error' );
			}

			showLoader();

			DataService.user().fetchAccountData({}, onFetch, onFetchError );
		}

		var onGetError = function( error ){
			$scope.uiState.isGettingAuthToken = false;
			Utils.log( ['error in getting fortnox auth token.',error], 'error' );

			if( helper.isExist(error.data.message ) ){
				Utils.toast( error.data.message,'error' );
			}

			$form.client_secret.$setValidity( 'required', false );
			$form.authorization_key.$setValidity( 'required', false );
		}

		if( ! $rootScope.gscope.user.connectedToFrotnox ){

			$scope.uiState.isGettingAuthToken = true;
			DataService.user().getAuthToken( $inputData, onGet, onGetError );
		}

	}

	function watchFortnoxProcess(){

		watchFortnoxProcessInstance = $interval(function(){

				DataService.user().get({ param:$rootScope.gscope.user.id }, function( response ){
					Utils.log(['Watch response', response]);
					if( parseInt( response.data.fortnox_process ) == 0 ){

						var cookie = Utils.getCookies();

						$interval.cancel( watchFortnoxProcessInstance );
						watchFortnoxProcessInstance = undefined;
						showSelloUrl();

						cookie.fortnox_process = 0;
						Utils.setCookies( cookie );
						$rootScope.gscope.user.fortnoxProcess = 0; 
						$scope.notes = [];
					}
					
				});
			},30000);
	}
	var showLoader = function(){
		
		$scope.uiState.isGettingAuthToken = true;
		var interval = 5000;
		var msgArray =  [   'Getting Connected to Fortnox.', 
							'Generating Authorization Code.',
							'Fetching Data from Fortnox server.' 
						];
		var i = 0;
		$scope.installMessage = msgArray[i++];
		$scope.stop = $interval( function(){

			$scope.installMessage = msgArray[i++];
			
			if( i == msgArray.length ){
				i = 0;
			}

		},interval);

		$timeout(function(){
			$interval.cancel( $scope.stop );
			$scope.uiState.isGettingAuthToken = false;
			fortnoxProcessingNotification();
			watchFortnoxProcess();
		}, ( interval * msgArray.length ) );

	}

	
	//showLoader();

	$scope.copy =  function(){
		if( ! $scope.selloUrl ){
			 return false;
		}
		new Clipboard('#sello-url');
		Utils.toast( "Copied to Clipboard.")
	}

	$scope.expiry = '12 / 2018';
	$scope.stripeEmail = $rootScope.gscope.user.email;

	$scope.stripeCallback = function (code, result) {

		var onSave = function( response ){
			
			var cookie = Utils.getCookies();
			Utils.log( [ 'on payment',response ] );
			cookie.stripe_customer = response.data.id;
			cookie.paid = 1;
			Utils.setCookies( cookie );
			Utils.setGlobalUserData();
			Utils.toast( response.message );
			$rootScope.isSubscribing = false;
		}

		var onSaveError = function( error ){
			Utils.log( [ 'on payment Error ',error ] );

			$rootScope.isSubscribing = false; 
			Utils.toast(  error.data.error , 'error' );
		}

	    if (result.error) {
			$rootScope.isSubscribing = false;

	    	Utils.toast( result.error , 'error' );
	    } else {
	    	
	    	DataService.stripe().save( { stripeToken:result.id,stripeEmail : $scope.stripeEmail }, onSave, onSaveError );
	    }
	}

}

app.controller( 'settingCtrl', settingCtrl );   