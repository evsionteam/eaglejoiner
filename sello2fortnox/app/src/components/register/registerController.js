var registerCtrl = function( $rootScope, $scope, DataService, Utils, helper ){

	$scope.uiState = {
		isRegistering : false
	}

	$scope.register = {
		gender :  'male'
	}

	$scope.doRegister = function(){

		var onRegister = function( response ){
			Utils.redirect( 'login' );
			Utils.toast(  'Registered successfully.' , 'success' );
		}

		var onRegisterError = function( error ){

			$scope.uiState.isRegistering = false;
			if( helper.isExist( error.data.message ) ){
				angular.forEach( error.data.message, function( value, key ){

					switch( key ){
						case 'email':
							
							$scope.registerForm.email.$setValidity("required", false);
						break;

						case 'username':
							
							$scope.registerForm.username.$setValidity("required", false);
						break;
					}

					Utils.toast( value, 'error' );
				});
			}
			
			Utils.log( [ 'Registeration Failed.', error ], 'error' );
		}

		if( $scope.register.password == $scope.register.confirmPassword ){

			var insert = {
				email : $scope.register.email,
				username : $scope.register.username,
				password : $scope.register.password
			}
			$scope.uiState.isRegistering = true;

			DataService.user().save( insert, onRegister, onRegisterError );
		}else{

			$scope.registerForm.password.$setValidity( 'required', false );
			$scope.registerForm.confirm_password.$setValidity( 'required', false );
			Utils.toast( 'Password does not match.', 'error' );
		}
	}
}
app.controller( 'registerCtrl', registerCtrl );