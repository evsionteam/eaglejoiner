var notification = function (SF, helper) {
    return {
        restrict: 'E',
        templateUrl: SF.sharedPath + '/notification/notificationView.html',
        scope: {
            notifications: '=message'
        },
        replace:true,
        link: function (scope, ele, atts) {

            scope.$watch( 'notifications',function(){ 
                angular.forEach(scope.notifications, function (value, key) {

                    var defaults = {
                        message: "No Content.",
                        showBtn: true,
                        btnText: 'Button',
                        btnLink: '#/',
                        class: '',
                        enable: true
                    }
                    
                    scope.notifications[key] = angular.extend(defaults, value);
                });
             },true)
          
        }
    }
}
notification.$inject = ['SF', 'helper'];
app.directive('notification', notification);