var DataService = function( $rootScope, $resource, $http, SF ){

	this.headers = {
		'authToken' : null
	}

	this.getMenu = function(){
		return $http.get( SF.srcPath + '/menu.json' ).then( function( response ){
			return response.data;
		})
	}

	this.getConfig = function(){
		return $http.get( SF.srcPath + '/config.json' ).then( function( response ){
			return response.data;
		})
	}

	this.login = function( data ){
	 	return $resource( SF.apiPath + '/user/login',{},{
	 		login : {
	 			method : 'POST'
	 		}
	 	}).login( data ).$promise;
	}

	this.user = function(){

		var actions = {
			query  : {
				method  : 'GET',
				headers : this.headers
			},
	 		get    : {
	 			method  : 'GET',
	 			headers : this.headers
	 		},
	 		update : {
	 			method  : 'PUT',
	 			headers : this.headers,
	 			params  : {
	 				param : 'update'
	 			}
	 		},
	 		resetLink : {
	 			method  : 'POST',
	 			params  : {
	 				param : 'reset'
	 			}
	 		},
	 		logout   : {
	 			method  : 'GET',
	 			params : {
	 				param : 'logout'
	 			},
	 			headers : this.headers
	 		},
	 		reset    : {
	 			method  : 'PUT',
	 			params  : {
	 				param : 'reset'
	 			}
	 		},
	 		isValid : {
	 			method : 'GET',
	 			params : {
	 				param : 'valid'
	 			},
	 			headers : this.headers
	 		},
	 		getAuthToken : {
				method  : "POST",
				headers :  this.headers,
				params : {
					param : 'get-fortnox-token'
				}
			},
			fetchAccountData : {
				method : "POST",
				headers : this.headers,
				params  : {
					param : 'all-fortnox-account'
				}
			},
			changePassword : {
				method : "PUT",
				headers : this.headers,
				params  : {
					param : 'changepassword'
				}
			}

	 	};

	 	return $resource( SF.apiPath + '/user/:param', { param : '@id' }, actions );
	}

	this.log = function(){

		var actions = {
			query : {
				method : 'GET',
				headers : this.headers
			},
			get : {
				method : 'GET',
				headers : this.headers
			},
			delete : {
				method : 'POST',
				headers : this.headers,
				params : {
					param1 : 'delete'
				}
			},deleteAll : {
				method : 'GET',
				headers : this.headers,
				params : {
					param1 : 'delete'
				}
			},
			filter : {
				method : 'POST',
				params  : {
					param1 : 'filter'
				},
				headers : this.headers
			},
			getAll : {
				method : 'GET',
				headers : this.headers,
				params : {
					param1 : 'export'
				}
			}
		}

		return $resource( SF.apiPath + '/activity/:param1/:param2',{ param1 : '@param1', param2 : '@param2' }, actions );
	}


	this.stripe = function(){

		var actions = {
			save : {
				method : "POST",
				headers : this.headers
			},
			get : {
				method : "GET",
				headers : this.headers
			}
		}

		return $resource( SF.apiPath + '/stripe/customer/:id',{ id : '@id' }, actions );
	}

	this.card = function(){
		
		var actions = {
			get : {
				method   : "GET",
				headers  : this.headers
			},
			save : {
				method  : "POST",
				headers : this.headers
			},
			update : {
				method  : 'PUT',
				headers : this.headers
			},
			delete : {
				method  : 'DELETE',
				headers : this.headers
			}
		}

		return $resource( SF.apiPath + '/stripe/card/:id/:id2', { id : '@id', id2 : '@id2' }, actions );
	}

	this.subscription = function(){

		var actions = {
			cancel : {
				method: 'POST',
				headers : this.headers,
				params : {
					url : 'unsubscribe',
					id  : '@id'
				}
			},
			list : {
				method: 'POST',
				headers : this.headers,
				params : {
					url : 'subscription',
					id  : '@id'
				}
			},
			query : {
				method : 'GET',
				headers : this.headers,
				params : {
					url : 'subscription',
					id : $rootScope.gscope.user.stripeCustomer
				}
			},
			update : {
				method : 'POST',
				headers : this.headers,
				params : {
					url : 'update',
					id  : 'subscription' 
				}
			}
		}

		return $resource( SF.apiPath + '/stripe/:url/:id', { url : '@url', id:'@id' }, actions );
	}

	

}
DataService.$inject = ['$rootScope', '$resource', '$http', 'SF'];
app.service( 'DataService', DataService );