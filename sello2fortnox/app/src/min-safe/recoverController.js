var recoverCtrl = function( $rootScope, $scope, DataService, Utils ){

	$scope.uiState = {
		hasResetLink       : false,
		isGettingResetLink : false,
		isRecovering       : false
	}

	$scope.resetLink = function(){

		var onResetLink = function( response ){
			$scope.uiState.isGettingResetLink = false; 
			$scope.uiState.hasResetLink = true;
			Utils.toast( response.message ,'success' );
		}

		var onResetLinkError = function( error ){

			$scope.resetLinkForm.email.$setValidity( 'required' , false );

			$scope.uiState.isGettingResetLink = false;
			Utils.toast( error.data.message , 'error' );
			Utils.log( [ 'Reset Password Link Error.', error ], 'error' );
		}

		$scope.uiState.isGettingResetLink = true;
		DataService.user().resetLink( $scope.reset, onResetLink, onResetLinkError );
	}

	$scope.recoverIt = function(){

		$scope.recover.email = $scope.reset.email;

		var onRecover = function( response ){

			$scope.uiState.isRecovering = false;
			Utils.toast( response.message , 'success' );
			
			Utils.redirect( 'login' );
		}

		var onRecoverError = function( error ){

			$scope.uiState.isRecovering = false;
			$scope.recoverForm.reset_token.$setValidity( 'required', false );
			
			Utils.toast( error.data.message , 'error' );
			Utils.log( [ 'Reset Password Error.', error ], 'error' );
		}

		$scope.uiState.isRecovering = true;
		DataService.user().reset( $scope.recover, onRecover, onRecoverError );
	}
}
recoverCtrl.$inject = ['$rootScope', '$scope', 'DataService', 'Utils'];
app.controller( 'recoverCtrl', recoverCtrl );