var LoginService = function( $resource , SF ){

	return  $resource( SF.apiPath + '/user/login', {},{
        login:{
            method:"POST"
        }
    });
}
LoginService.$inject = ['$resource', 'SF'];

app.factory( 'LoginService', LoginService );
