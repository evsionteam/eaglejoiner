var loader = function ( $rootScope, Utils, helper, SF ) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: SF.sharedPath + "/loader/loaderView.html",
        link: function (scope, elem, attrs) {

            scope.isLoadedFirstTime = true;
            scope.isRouteLoading = false;

            var exception = [ '/login','/register','/recover' ];

            $rootScope.$on('$routeChangeStart', function ( e , next, current ) {

                if( exception.indexOf(current.$$route.originalPath) != -1 ){
                    scope.isRouteLoading = false;
                    return;
                }

                scope.isRouteLoading = true;
            });

            $rootScope.$on('$routeChangeSuccess', function () {

                scope.isRouteLoading = false;

                if( scope.isLoadedFirstTime ){
                    
                    scope.isLoadedFirstTime = false;
                }
            });

            $rootScope.$on('$routeChangeError', function () {
                scope.isRouteLoading = false;
                Utils.log("Route Change Error.", 'error');
            })
        }
    };
};
loader.$inject = ['$rootScope', 'Utils', 'helper', 'SF'];

app.directive('loader', loader);