var topHeader = function (SF, Utils,$window) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: SF.sharedPath + '/topheader/topHeaderView.html',
        controller: ['$scope', function ($scope) {

            var windowWidth = $(window).width(),
                $menu = $('.menu-icon');
        
                if (windowWidth < 767) {
                    $menu.trigger('click');
                }

            $scope.logout = function () {

                Utils.logout();
            }
            
            
            
        }]
    }
}
topHeader.$inject = ['SF', 'Utils', '$window'];
app.directive('topHeader', topHeader);