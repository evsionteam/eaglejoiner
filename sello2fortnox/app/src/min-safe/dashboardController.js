var dashboardCtrl = function( $rootScope, $scope, DataService, Utils, logs ){

	$rootScope.gscope.pageTitle   = 'Dashboard';
	$rootScope.gscope.pageHeading = 'Dashboard';
	$scope.log = logs;
	
	var fortnoxProcess = parseInt($rootScope.gscope.user.fortnoxProcess);
        fortnoxProcess = ( fortnoxProcess == 0 || fortnoxProcess == 1  ) ? fortnoxProcess : undefined;
	
	$scope.selloStatus = false;
	if( $rootScope.gscope.user.fortnoxAuthorizationToken && fortnoxProcess == 0 ){
		$scope.selloStatus = true;
	}

	$scope.notes = [{
		message 	: "1Since you still have one more steps to complete, you won't be able to give you full fledged services. To unlock complete feature of sello 2 fortnox, please click here.",
		showBtn 	: true,
		btnText 	: 'Subscribe',
		btnLink 	: '#/setting',
		enable      : ! $rootScope.gscope.user.stripeCustomer
	}];

}
dashboardCtrl.$inject = ['$rootScope', '$scope', 'DataService', 'Utils', 'logs'];
app.controller( 'dashboardCtrl', dashboardCtrl );
// 