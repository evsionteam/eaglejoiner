module.exports = function( grunt ){
	
	grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        ngAnnotate: {
		    options: {
		        singleQuotes: true
		    },
		    app: {
		        files: {
		            './app/src/min-safe/app.js' : ['./app/src/app.js'],
		            './app/src/min-safe/utils.js' : ['./app/src/shared/utils.js'],
		            './app/src/min-safe/resource.js' : ['./app/src/shared/resource.js'],
		            './app/src/min-safe/loaderDirective.js' : ['./app/src/shared/loader/loaderDirective.js'],
		            './app/src/min-safe/jqueryDirectives.js' : ['./app/src/shared/jqueryDirectives.js'],
		            './app/src/min-safe/notificationDirective.js' : ['./app/src/shared/notification/notificationDirective.js'],

		            './app/src/min-safe/loginController.js' : ['./app/src/components/login/loginController.js'],
		            './app/src/min-safe/registerController.js' : ['./app/src/components/register/registerController.js'],
		            './app/src/min-safe/recoverController.js' : ['./app/src/components/recover/recoverController.js'],
		            './app/src/min-safe/dashboardController.js' : ['./app/src/components/dashboard/dashboardController.js'],
		            './app/src/min-safe/settingController.js' : ['./app/src/components/setting/settingController.js'],
		            './app/src/min-safe/pagenotfoundController.js' : ['./app/src/components/pagenotfound/pagenotfoundController.js'],
		            './app/src/min-safe/logController.js' : ['./app/src/components/log/logController.js'],
		            './app/src/min-safe/editProfileController.js' : ['./app/src/components/editprofile/editProfileController.js'],
		            
		            './app/src/min-safe/sidemenuDirective.js' : ['./app/src/shared/sidemenu/sidemenuDirective.js'],
		            './app/src/min-safe/topHeaderDirective.js' : ['./app/src/shared/topheader/topHeaderDirective.js'],
		        }
		    }
		},
		concat: {
			options: {
			    separator: '\n/*next file*/\n\n'  //this will be put between conc. files
			},
		    js: { //target
		        src: [ './app/src/intro.js', './app/src/min-safe/app.js', './app/src/min-safe/*.js', './app/src/outro.js' ],
		        dest: './app/dist/app.js'
		    }
		},
		uglify: {
		    js: { //target
		        src: ['./app/dist/app.js'],
		        dest: './app/dist/app.min.js'
		    }
		},
		watch: {
			js : {
				files : ['./app/src/**/*.js'],
				tasks : [ 'default' ]
			}
		} 
    });

    //load grunt tasks
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-ng-annotate'); 

    //register grunt default task
    grunt.registerTask('default', ['ngAnnotate', 'concat', 'uglify']);
}