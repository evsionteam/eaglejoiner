
$(document).ready(function () {
    // Nice scroll
    $('html').niceScroll({
        cursorcolor: "#1E90B3",
        background: "#ccc",
        cursoropacitymin: 0.4,
        cursorwidth: "6px",
        cursorborder: "none",
        cursorborderradius: "0"
    });

    // focus search input
    $(document).on('click', '.search .search-icon', function () {
        $('.search-ani input').focus();
    });

    // filter 
    $(document).on('click', '#filer-icon', function () {
        $('.filter-options').slideToggle(180);
    });

});


// HANDLE CUSTOM FILE FIELD: UPDATE FILE NAME
// -----------------------------------------
$(window).ready(function () {

    $('.custom-import').each(function (i, node) {
        $(node).find('input').change(function () {
            var value = $(this).val();
            if (value === "")
                return;

            var extension = value.split('\\').pop();
            $('.label').hide();
            $(node).find('.label-file').html(extension);
        });
    });

});