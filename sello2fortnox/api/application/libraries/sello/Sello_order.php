<?php
defined('BASEPATH') OR exit('No direct script access allowed');


require_once APPPATH . 'libraries/sello/Sello-Common.php';
// This can be removed if you use __autoload() in config.php OR use Modular Extensions

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 */
class Sello_order {

	protected $api_key;
	protected $endpoint;
	protected $content_type ;
	protected $accepts ;
	protected $access_token ;
	protected $headers;
	protected $url = "order";

	
	use sello_common;
	
	
	public function get ( $order_no = FALSE ) { 
		$url = $this->endpoint . $this->url;

		if( $order_no ) {
			$url = $url ."/".$order_no;		
		}
		
		$response = Requests::get($url, $this->headers );
		return json_decode($response->body);

	}

	public function get_customer(){
		
		$url = $this->endpoint . "order/customer";
		$response = Requests::get($url, $this->headers );

		return json_decode($response->body);
	}

	public function create( $args ) {
    		
    }

}