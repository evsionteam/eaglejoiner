<?php
trait sello_common {

	public function __construct( $args ){
		/**
		 * Set params
		 **/
        $this->endpoint = "https://api.sello.io/v3/";
		$this->content_type = "application/json";
		$this->accepts = "application/json";

		$this->access_token = $args['access_token'];
		$this->api_key = $args['api_key'];

		/**
		 * Prepare headers
		 **/
		$this->headers = array(
				'Token'=> $this->access_token,
				'Key' => $this->api_key,
				// 'Content-Type' => $this->content_type,
				// 'Accept' => $this->accepts
			);
	}

	public function get_access_token( $client_secret, $authorization_code ){
		/*$headers = array(
		  'Authorization-Code'	=> $authorization_code,
		  'Key' 		=> $api_key,

		  'Content-Type' 		=> $this->content_type,
		  'Accept'				=> $this->accepts
		 );*/
		
		$headers= array();

		$response = Requests::get( $this->endpoint, $headers );

		return json_decode($response->body);
	}

}