<?php
defined('BASEPATH') OR exit('No direct script access allowed');


require_once APPPATH . 'libraries/fortnox/Fortnox-Common.php';
// This can be removed if you use __autoload() in config.php OR use Modular Extensions

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 */
class Fortnox_order {

	protected $url = "orders";
	protected $api_code;
	protected $client_secret;

	protected $endpoint;
	protected $content_type ;
	protected $accepts ;
	protected $access_token ;
	protected $headers;
	
	use fortnox_common;
	
	
	public function get ( $order_id = FALSE ) { 

		$url = $this->endpoint.$this->url;
		
		if( $order_id ) {
			$url = $url . "/". $order_id;
		}

		$response = Requests::get($url, $this->headers);

		return json_decode($response->body);

	}


	public function create( $args ) {
        $body = [
            "Order" => $args
        ];

		return $response = Requests::post($this->endpoint.$this->url, $this->headers, json_encode($body));

    }

    public function update( $order_no, $args ) {
        $body = [
            "Order" => $args
        ];
        
        $url = $this->endpoint.$this->url. "/".$order_no;

		return $response = Requests::put($url, $this->headers, json_encode($body));

    }


    public function create_account( $args ) {
    	$body = [
            "Account" => $args
        ];

		return $response = Requests::post($this->endpoint.'accounts', $this->headers, json_encode($body));
    }

    public function create_invoice( $order_id, $body = array() ){
    	$url = $this->endpoint.$this->url. '/'.$order_id.'/createinvoice' ;
    	return $response = Requests::put( $url, $this->headers, json_encode($body));
    }
    
    public function order_cancel( $order_id ) {
    	$url = $this->endpoint.$this->url. '/'.$order_id.'/cancel' ;
    	return $response = Requests::put( $url, $this->headers);
    }

    public function bookkeep( $invoice_id ) {
		$url = $this->endpoint."invoices/".$invoice_id .'/bookkeep';
		return $response = Requests::put($url, $this->headers, json_encode( array() ));    	
    }
}