<?php
defined('BASEPATH') OR exit('No direct script access allowed');


require_once APPPATH . 'libraries/fortnox/Fortnox-Common.php';
// This can be removed if you use __autoload() in config.php OR use Modular Extensions

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 */
class Fortnox_invoice {

	protected $url = "invoices";
	protected $api_code;
	protected $client_secret;

	protected $endpoint;
	protected $content_type ;
	protected $accepts ;
	protected $access_token ;
	protected $headers;
	
	use fortnox_common;
	
	
	public function get ( $invoice_id = FALSE ) { 

		$url = $this->endpoint.$this->url;
		
		if( $invoice_id ) {
			$url = $url . "/". $invoice_id;
		}

		$response = Requests::get($url, $this->headers);

		return json_decode($response->body);

	}


	public function create( $args ) {
        $body = [
            "Invoice" => $args
        ];

		return $response = Requests::post($this->endpoint.$this->url, $this->headers, json_encode($body));

    }

    public function update( $invoice_id, $args ) {
        $body = [
            "Invoice" => $args
        ];
        
        $url = $this->endpoint.$this->url. "/".$invoice_id;

		return $response = Requests::put($url, $this->headers, json_encode($body));

    }

    public function bookkeep( $invoice_id ) {
		$url = $this->endpoint.$this->url. "/".$invoice_id .'/bookkeep';
		return $response = Requests::put($url, $this->headers, json_encode( array() ));    	
    }
}