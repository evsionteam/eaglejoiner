<?php
defined('BASEPATH') OR exit('No direct script access allowed');


require_once APPPATH . 'libraries/fortnox/Fortnox-Common.php';
// This can be removed if you use __autoload() in config.php OR use Modular Extensions

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 * http://developer.fortnox.se/documentation/resources/accounts/#Retrieve-an-account
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 */
class Fortnox_accounts {

	protected $url = "accounts";
	protected $api_code;
	protected $client_secret;

	protected $endpoint;
	protected $content_type ;
	protected $accepts ;
	protected $access_token ;
	protected $headers;
	
	use fortnox_common;
	
	
	public function get ( $account_no = FALSE, $response = FALSE ) { 

		$url = $this->endpoint.$this->url;
		
		if( $account_no ) {
			$url = $url . "/". $account_no;
		}

		$response = Requests::get( $url, $this->headers );
		
		if( $response )
			return $response;

		return json_decode( $response->body );

	}


	public function create( $args ) {
        $body = [
            "Account" => $args
        ];

		return $response = Requests::post($this->endpoint.$this->url, $this->headers, json_encode($body));

    }

    public function update( $order_no, $args ) {
        $body = [
            "Account" => $args
        ];
        
        $url = $this->endpoint.$this->url. "/".$order_no;

		return $response = Requests::put($url, $this->headers, json_encode($body));

    }

    public function get_fortnox_vat_code( $vat ) {
		$vat_code = '';
		
		switch( intval( $vat ) ) {
			case 25:
				$vat_code = "MP1"; 
				break;

			case 12:
				$vat_code = "MP2";
				break;

			case 6:
				$vat_code = "MP3";
				break;

			default:
				$vat_code = '';
		}

		return $vat_code;
	}


	public function get_fortnox_vat_percent( $code ) {
		$vat_percent = 0;
		
		switch(  $code  ) {
			case "MP1":
				$vat_percent = 25; 
				break;

			case "MP2":
					$vat_percent = 12;
				break;

			case "MP3":
				$vat_percent = 6;
				break;

			default:
				$vat_percent = 0;
		}

		return $vat_percent;
	}


}