<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'libraries/fortnox/Fortnox-Common.php';
// This can be removed if you use __autoload() in config.php OR use Modular Extensions

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 */
class Fortnox_item {

	protected $url;
	protected $api_code;
	protected $client_secret;

	protected $endpoint;
	protected $content_type ;
	protected $accepts ;
	protected $access_token ;
	protected $headers;

	
	use fortnox_common;
	
	
	public function get ( $entity ) { 

		$url = $this->endpoint . "articles";

		$response = Requests::get($url, $this->headers);

		return json_decode($response->body);

	}


	public function create( $args ) {
        $body = [
            "Article" => $args
        ];

		return $response = Requests::post($this->endpoint. 'articles', $this->headers, json_encode($body));

    }
    /**
     * Prices 
     * https://api.fortnox.se/3/prices
     **/
    public function create_price( $ArticleNumber, $price, $FromQuantity =1 ) {
    	$body = array(
    			'Price' => array(
    					'ArticleNumber'	=> $ArticleNumber,
    					'FromQuantity'	=> $FromQuantity,
    					'Price'			=> $price,
    					'PriceList'		=> 'A',

    				)
    		);

    	return $response = Requests::post($this->endpoint. 'prices', $this->headers, json_encode($body));
    }

    /**
     * @param $id
     * @param $args
     * @return mixed|object
     */
    public function update( $id, $args ) {
        $url = $this->endpoint . "articles". '/'. $id;
        $body = [
            "Article" => $args
        ];

        return $response = Requests::put($this->endpoint. 'articles', $this->headers, json_encode($body));
    }

}