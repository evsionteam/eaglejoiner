<?php
trait fortnox_common {

	public function __construct( $args = array() ){
		// parent::__construct();

		/**
		 * Set params
		 **/
        $this->endpoint = "https://api.fortnox.se/3/";
		$this->content_type = "application/json";
		$this->accepts = "application/json";
		if( !empty( $args )){
			$this->access_token = $args['access_token'];
			$this->client_secret = $args['client_secret'];
		}

		/**
		 * Prepare headers
		 **/
		$this->headers = array(
				'Access-Token'=> $this->access_token,
				'Client-Secret' => $this->client_secret,
				'Content-Type' => $this->content_type,
				'Accept' => $this->accepts
			);
	}

	public function get_access_token( $client_secret, $authorization_code ){
		$headers = array(
		  'Authorization-Code'	=> $authorization_code,
		  'Client-Secret' 		=> $client_secret,

		  'Content-Type' 		=> $this->content_type,
		  'Accept'				=> $this->accepts
		 );

		return $response = Requests::get( $this->endpoint, $headers );

		return json_decode($response->body);
	}

}