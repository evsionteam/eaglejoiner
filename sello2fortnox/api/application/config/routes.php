<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] 		= 'welcome';
$route['404_override'] 				= 'welcome/page_404';
$route['translate_uri_dashes'] 		= FALSE;

$route['stripe'] 					= "welcome/stripe";

// $route['news/create'] = 'news/create';
// $route['news/(:any)'] = 'news/view/$1';
// $route['news'] = 'news';
// $route['(:any)'] = 'pages/view/$1';
// $route['default_controller'] = 'pages/view';


/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/
$route['user/login']['POST'] 				= 'api/users/login';
$route['user/logout']['GET'] 				= 'api/users/logout';
$route['user/valid']['GET'] 				= 'api/users/is_valid';
$route['user/update']['PUT'] 				= 'api/users/update';
$route['user/reset']['POST'] 				= 'api/users/send_password_reset_link';
$route['user/reset']['PUT'] 				= 'api/users/reset_password';
$route['user/changepassword']['PUT'] 		= 'api/users/chnage_password';


//$route['api/user']['GET'] 					= 'api/users/current';
$route['user/(:num)']['GET'] 				= 'api/users/get/$1';
$route['user']['GET'] 						= 'api/users/get';
$route['user']['POST'] 						= 'api/users/post';

$route['user/url']['GET'] 					= 'api/users/get_unique_url';
$route['user/url']['PUT'] 					= 'api/users/update_unique_url';

$route['user/all-fortnox-account']['POST'] 	= 'api/users/get_all_fortnox_accoutns';
$route['user/get-fortnox-token']['POST'] 	= 'api/users/get_fortnox_token';

/**
 * Activity
 **/
$route['activity/receiver/(:any)']['POST'] 	= 'api/activity/activity/$1';
$route['activity/filter']['POST'] 			= 'api/activity/filter';
$route['activity/filter/(:num)']['POST'] 	= 'api/activity/filter/$1';

$route['activity/delete']['POST'] 			= 'api/activity/delete';
$route['activity/delete']['GET'] 			= 'api/activity/delete_all';

$route['activity']['PURGE'] 				= 'api/activity/truncate';
$route['activity/(:num)']['GET'] 			= 'api/activity/get';
$route['activity']['GET'] 					= 'api/activity/get';
$route['activity/export']['GET'] 			= 'api/activity/export';

/** 
 * Stripe
 **/
$route['stripe/customer']['GET']			= 'api/stripe_api/get_customer';
$route['stripe/customer/(:any)']['GET']		= 'api/stripe_api/get_customer';
$route['stripe/customer']['POST']			= 'api/stripe_api/create_customer';

$route['stripe/card/(:any)']['GET']			= 'api/stripe_api/get_cards';
$route['stripe/card']['POST']				= 'api/stripe_api/create_card';
$route['stripe/card']['PUT']				= 'api/stripe_api/update_card';

//stripe info customer_id, card_id
$route['stripe/card/(:any)/(:any)']['DELETE']			= 'api/stripe_api/delete_card';
$route['stripe/subscription/(:any)']['GET']				= 'api/stripe_api/subscription_all';
$route['stripe/unsubscribe/(:any)']['POST']				= 'api/stripe_api/unsubscribe';
$route['stripe/update/subscription']['POST']			= 'api/stripe_api/again_subscription';

//web hook receiver from stripe 
$route['stripe/webhook']['POST']						= 'api/stripe_api/webhook_receiver';

// $route['api/users'] = 'api/users';
// $route['api/users/(:num)'] = 'api/users/$1'; // Example 4
// $route['api/example/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4'; // Example 8
