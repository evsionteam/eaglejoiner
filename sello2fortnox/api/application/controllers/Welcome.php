<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		/*$this->config->load('stripe');
		if( $this->config->item('stripe_test_mode')){
			$stripe_api_key = $this->config->item('stripe_key_test_secret');
		}else {
			$stripe_api_key = $this->config->item('stripe_key_live_secret');
		}
		
		\Stripe\Stripe::setApiKey( $stripe_api_key );

		$customer = \Stripe\Customer::retrieve("cus_90eZzVbRWWoV8B");
		// $customers = \Stripe\Customer::all(array("limit" => 3));

		echo '<pre>';
		$customer_array = $customer->__toArray(true);
		print_r($customer_array);*/


// exit;
		$this->load->library( 'stripe/stripe' );
		// $customer = array('name');
		// $customer = json_decode( $this->stripe->customer_list() );
		// echo '<pre>';
		// print_r($customer );
		// exit;

		// $args = array(
		//     'number'    => '4242424242424242',
		//     'exp_month' => 10,
		//     'cvc'       => 314,
		//     'exp_year'  => 2020
		//  );
		// $card = $this->stripe->create_card("cus_8zVt87AsyB4zZj", $args);
		// echo '<pre>';
		// print_r($card); exit;
		
		$this->load->view('welcome_message');
	}

	public function page_404(){
		die("404");
	}


	public function stripe( ) {
		echo '<pre>';
		$this->load->library( 'stripe/stripe' );
		$this->load->model( 'stripe_model' );
		// $customer = array('name');
		// $customer = json_decode( $this->stripe->invoice_list() );

		print_r($this->input->post());

		$token 			= $this->input->post('stripeToken');
		$tokenType 		= $this->input->post('stripeTokenType');
		$email 			= $this->input->post('stripeEmail');

		exit;
		$customer = json_decode( $this->stripe->customer_create( $token , $email, '', 'test_plan'));

		$data = array(
				'user_id' 		=> 9, 
				'stripe_id' 	=> $customer->id, 
				'email'			=> $customer->email,
				'currency'		=> $customer->currency,
				'subscriptions'	=> json_encode( $customer->subscriptions->data ),
				'response'		=> json_encode( $customer )
			);

		print_r($customer);

		echo $insert = $this->stripe_model->insert( $data );

	}
}
