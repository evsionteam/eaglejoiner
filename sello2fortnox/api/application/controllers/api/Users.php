<?php
defined('BASEPATH') OR exit('No direct script access allowed');
set_time_limit(0);

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 */
class Users extends API_Controller {
	public $upload_dir 							= FCPATH. '/uploads/';
	public $user_avatar_dir 					= 'uploads/avatar/';
	public $user_avatar_url 					= ''; 

	private $message_email_exits 				= "Email address already in used!";
	private $message_username_exits 			= "Username address already in used!";
	private $message_crete 						= "User successfully created!";
	private $message_update 					= "User successfully updated!";
	private $message_not_update 				= "User not updated, try again!";
	private $message_delete 					= "User successfully deleted!";
	private $message_login_success 				= "You have successfully logged in";
	private $message_login_error 				= "You have entered invalid data. Please try again";
	private $message_login_username_error 		= "Invalid Username !";
	private $message_login_password_error 		= "Invalid Password !";

	private $message_email_send 				= "Please check your email to reset password.";
	private $message_email_not_send 			= "Email not send, please try again";
	private $message_email_not_valid 			= "Email address is not valid";
	private $message_password_reset 			= "Password reset successfully complete";
	private $message_password_not_reset 		= "Password reset not done, please try again.";
	private $message_user_not_found 			= 'User not found';
	private $message_token_not_valid 			= 'Reset token is not valid';
	private $message_some_thing_error 			= "Something error, please try again!";				

	public function __construct()
	{
		parent::__construct();
		$this->user_avatar_url = base_url().  $this->user_avatar_dir ;

		$this->load->model('users_model');
		$this->load->model("users_meta_model");
		$this->load->model("accounts_model");
		$this->validate();

		$this->load->library('encrypt');
		$this->load->helper('email');

	}

	/**
	 * @api {post} /user/login User login
	 * @apiName Login User
	 * @apiGroup User
	 *
	 * @apiSuccess {Object} Return User object.
	*/
	public function login(){

		$input_data = json_decode(trim(file_get_contents('php://input')), true);

		if(! $this->validate_inputs($input_data)){
			return $this->response(["message" =>API_Controller::MESSAGE_INVALID_DATAFORMAT], API_Controller::HTTP_FORBIDDEN);
		}
		
		$input_data = (object) $input_data;

		$user = $this->users_model->get_user_by_email($input_data->email);
		
		$data['data'] = array();
		
		if( !$user ){
			$user = $this->users_model->get_user_by_username($input_data->email);
		}

		if( $user ) {
			$password = $this->encrypt->decode($user->password);

			if( $input_data->password == $password ) {
				
				$data['data'] = $this->_get_user_with_meta( $user );
				$data['message'] = $this->message_login_success;

				return $this->response($data);

			} else {
				$data['field']['password'] = $this->message_login_password_error;
			}

		} else {
			$data['field']['username'] = $this->message_login_username_error;
		}

		return $this->response($data, API_Controller::HTTP_UNAUTHORIZED);	
	}

	public function _get_user_with_meta( $user ){
		$user_meta = $this->users_meta_model->get_user_meta($user->id);;
		foreach( $user_meta as $meta ){
			$user->{$meta['meta_key']} = $meta['meta_value'];
		}
		
		$user->user_unique_url = $this->_unique_url( $user->id );
		$user->stripe = $this->_get_stripe_info( $user->id );

		return $user;
	}

	/**
	 * @api {get} /user/logout User logout
	 * @apiName Logout User
	 * @apiGroup User
	 *
	 * @apiSuccess {String} Return Message.
	*/
	public function logout( ) {
		if( !$this->is_valid_user ){
			return $this->response(['error'=> API_Controller::MESSAGE_INVALID_USER ], API_Controller::HTTP_FORBIDDEN );
		}

		$new_token = generate_token( $this->current_user->id );
		$update = $this->users_meta_model->update_user_meta( $this->current_user->id, 'auth_token', $new_token);

		if( $update ){
			$data["message"] = "Successfully logout!";
			$data['data'] = NULL;
			return $this->response( $data );
		} else {
			$data['message'] = $this->message_some_thing_error;
			return $this->response( $data );
		}
	}

	/**
	 * @api {get} /user/valid User token validate
	 * @apiName Token Validate
	 * @apiGroup User
	 *
	 * @apiSuccess {String} Return Message.
	*/
	public function is_valid() {
		if( !$this->is_valid_user ){
			$data['message']  	= API_Controller::MESSAGE_INVALID_USER; 
			$data['status'] 	= API_Controller::HTTP_FORBIDDEN;
			return $this->response( $data );
		}
		
		$data['message']  	= "Success";
		$data['status'] 	= API_Controller::HTTP_OK;
		return $this->response( $data );
	}

	/**
	 * @api {get} /user/ Request User information
	 * @apiName CurrentUser
	 * @apiGroup User
	 *
	 * @apiSuccess {Object} Return User object.
	*/
	public function current(){
		if( !$this->is_valid_user ){
			return $this->response(['error'=> API_Controller::MESSAGE_INVALID_USER ], API_Controller::HTTP_FORBIDDEN );
		}

		$data['data'] 		= $this->current_user;
		$data['message'] 	= "Success";

		return $this->response($data);
	}

	/**
	 * @api {get} /user/:id Request User information
	 * @api {get} /users/ Return All user objects
	 * @apiName User Info
	 * @apiGroup User
	 *
	 * @apiParam {Number} id Users unique ID.
	 * @apiSuccess {Object} Return User object.
	 */
	public function get( $id = NULL ) {
		if( !$this->is_valid_user ){
			return $this->response(['error'=> API_Controller::MESSAGE_INVALID_USER ], API_Controller::HTTP_FORBIDDEN );
		}

		if( $id === NULL ) {
			// get all users list
			$users = $this->users_model->get_users();
			return $this->response(['message' => 'success', 'data' => $users, 'status' => 200]);
		}

		$id = (int) $id;
        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            return $this->response(["message" => API_Controller::MESSAGE_INVALID_REQUEST], API_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

		$user = $this->users_model->get_users($id);
		
		if( $user ){
			$user_meta = $this->users_meta_model->get_user_meta($user->id);;
			foreach( $user_meta as $meta ){
				$user->{$meta['meta_key']} = $meta['meta_value'];
			}
			
			$user->user_unique_url =$this->_unique_url( $user->id );
			unset($user->meta->auth_token);
			
			$data = array();
			$data['message'] 	= 'success';
			$data['data'] 		=  $user;
			$data['status']		= API_Controller::HTTP_OK;
			return $this->response( $data );
		}

		$data = array();
		$data['message']  	= $this->message_user_not_found ;
		$data['status']  	= API_Controller::HTTP_NOT_FOUND;
		return $this->response( $data );
	}

	/**
	 * @param $data
	 * @return object
     */
	public function validate_users($data ){
		$errors  = array();
		$data['is_valid']	= true;
		
		if( isset($data['email'])) {
			if(!valid_email($data['email'])){
				$errors['email'] = $this->message_email_not_valid;	
				$data['is_valid']	= false;

			}else{
				$user = $this->users_model->get_user_by_email($data['email']);
				if( $user ){
					$errors['email'] = $this->message_email_exits;
					$data['is_valid']	= false;
				}
			}

		}

		if( isset($data['username'])){
			$user = $this->users_model->get_user_by_username($data['username']);
			
			if( $user ){
				$errors['username'] = $this->message_username_exits;
				$data['is_valid']	= false;
			}
		}

		$data['errors'] = $errors;

		return (object) $data;

	}

	

	/**
	 * @api {post} /user/create Create New User 
	 * @apiName Register User
	 * @apiGroup User
	 *
	 * @apiSuccess {Object} Return User object.
	 */
	public function post(){
		/*if( !$this->is_valid_user ){
			return $this->response(['error'=> API_Controller::MESSAGE_INVALID_USER ], API_Controller::HTTP_FORBIDDEN );
		}*/

		$input_data = json_decode(trim(file_get_contents('php://input')), true);
		
		$this->valid_input_keys = array('email', 'username', 'password');

		if(! $this->validate_inputs($input_data)){
			return $this->response(["message" =>API_Controller::MESSAGE_INVALID_DATAFORMAT], API_Controller::HTTP_FORBIDDEN);
		}

		$validate_data = $this->validate_users($input_data);

		if( $validate_data->is_valid){
			$created_id = $this->users_model->set_users($validate_data);
			if( $created_id ){
				$new_token = generate_token($created_id);

				$this->users_meta_model->update_user_meta($created_id, 'auth_token', $new_token);
				
				$this->users_meta_model->update_user_meta($created_id, 'user_unique_url', get_unique_string());
				$user = $this->users_model->get_users( $created_id );

				$data = array('user' => $user->username);

				$email_template = $this->load->view('email-template/welcome.php', $data, TRUE);

				$send = eagle_send_mail( $user->email, 'Welcome To Sello2Fortnox', $email_template );
				
				return $this->response(["message" => $this->message_crete], API_Controller::HTTP_OK);
			}

			else{
				return $this->response(["message"=>API_Controller::MESSAGE_INVALID_REQUEST], API_Controller::HTTP_INTERNAL_SERVER_ERROR);
			}

		}else{
			return $this->response(["message" =>$validate_data->errors], API_Controller::HTTP_FORBIDDEN);
		}
	}

	/**
	 * @api {put} /user/update Update User's info 
	 * @apiName Update User
	 * @apiGroup User
	 * @apiParam {Number} id Users unique ID.
	 * @apiSuccess {Object} Return User object.
	 */
	public function update(){

		if( !$this->is_valid_user ){
			return $this->response(['error'=> API_Controller::MESSAGE_INVALID_USER ], API_Controller::HTTP_FORBIDDEN );
		}
		
		$input_data = json_decode(trim(file_get_contents('php://input')), true);

		if(! $this->validate_inputs($input_data)){
			$data["message"] = API_Controller::MESSAGE_INVALID_DATAFORMAT;
			$data['status'] = API_Controller::HTTP_FORBIDDEN;

			return $this->response( $data );
		}

		$update = false;
		
		$input_data = (object) $input_data;
		
		$this->db->trans_start();

		if( isset($input_data->new_password) && $input_data->new_password != $this->encrypt->decode($this->current_user->password) ) {
			$new_password = $this->encrypt->encode( $input_data->new_password );
			$update_data = array(
					"data" => array('password'=> $new_password)
				);
			$update = $this->users_model->update_user($this->current_user->id, (object)$update_data);
			
		}
		
		if( isset($input_data->meta) ) {
			if( in_array('user_avatar', array_keys( $input_data->meta ) ) ){
				$user_avatar = $input_data->meta['user_avatar'];

				// create directory if not exits
				if (!is_dir($this->user_avatar_dir ) ) {
				    mkdir( $this->user_avatar_dir, 0777, TRUE);
				}

				// prepare file path for user
				$file_name = $this->current_user->id. '_'. $this->current_user->username. "_".time();
				$file_path = $this->user_avatar_dir. $file_name;
				
				// upload base 64 image				
				$upload_file_extenson = base64_to_file( $user_avatar, $file_path ); 
				unset( $input_data->meta['user_avatar']);
				
				if( $upload_file_extenson ) {
					// delete previous file
					if( isset( $this->current_user->meta_data->user_avatar ) ) {
						$previous_img = $this->current_user->meta_data->user_avatar;
						unlink( $previous_img );
					}

					$input_data->meta['user_avatar'] = $this->user_avatar_dir. $file_name.".{$upload_file_extenson}";
				}

			}

			foreach( $input_data->meta as $key=>$value ) {
				$update = $this->users_meta_model->update_user_meta($this->current_user->id, $key, $value);
			}
		}
		
		$this->db->trans_complete(); // transaction complete status

		if( $update ) {
			$data["message"] 	= $this->message_update;
			$data['status'] 	= API_Controller::HTTP_OK;
			$data['data']		= $this->_get_user_with_meta( $this->current_user );
			return $this->response( $data );
		}
		
		$data["message"] 	= $this->message_not_update;
		$data['status'] 	= API_Controller::HTTP_FORBIDDEN;
		
		return $this->response( $data );
	}

	/**
	 * @api {put} /user/changepassword Reset Users password 
	 * @apiName Reset Password
	 * @apiGroup User
	 * @apiParam {old_password, new_password}
	 * @apiSuccess {Object} Return User object.
	 */
	public function chnage_password() {
		if( !$this->is_valid_user ){
			return $this->response(['error'=> API_Controller::MESSAGE_INVALID_USER ], API_Controller::HTTP_FORBIDDEN );
		}
		
		$input_data = json_decode(trim(file_get_contents('php://input')), true);
		$this->valid_input_keys = array('old_password', 'new_password');
		if(! $this->validate_inputs($input_data)){
			return $this->response(["message" =>API_Controller::MESSAGE_INVALID_DATAFORMAT], API_Controller::HTTP_FORBIDDEN);
		}
		
		$update = false;
		
		$input_data = (object) $input_data;
		
		if( $this->encrypt->decode( $this->current_user->password ) != $input_data->old_password ){
			$response_data ['message'] 	= "Old password not match!";
			$response_data['data'] 		= NULL; 
			$response_data['status'] 	= API_Controller::HTTP_UNAUTHORIZED;

			return $this->response( $response_data );
		}

		$update = false;
		
		$input_data = (object) $input_data;
		
		$this->db->trans_start();

		if( isset($input_data->new_password) && $input_data->new_password != $this->encrypt->decode($this->current_user->password) ) {
			$new_password = $this->encrypt->encode( $input_data->new_password );
			$update_data = array(
					"data" => array('password'=> $new_password)
				);
			$update = $this->users_model->update_user($this->current_user->id, (object)$update_data);
			
			if( $update ) {
				$new_token = generate_token( $this->current_user->id );
				$this->users_meta_model->update_user_meta( $this->current_user->id, 'auth_token', $new_token);
			}

		}

		$this->db->trans_complete(); // transaction complete status


		$response_data ['message'] 					= "Password change successfully!";
		$response_data['data']['new_token'] 		= $new_token; 
		$response_data['status'] 					= API_Controller::HTTP_OK;

		return $this->response( $response_data );
	}

	/**
	 * @api {post} /user/reset Reset Users password 
	 * @apiName Reset Password
	 * @apiGroup User
	 * @apiParam {String} email Users unique ID.
	 * @apiSuccess {Object} Return User object.
	 */

	public function send_password_reset_link(){
		
		$input_data = json_decode(trim(file_get_contents('php://input')), true);
		if(! $this->validate_inputs($input_data)){
			return $this->response(["message" =>API_Controller::MESSAGE_INVALID_DATAFORMAT], API_Controller::HTTP_FORBIDDEN);
		}

		$input_data = (object) $input_data;

		$email = $input_data->email;

		if(isset( $email ) and valid_email( $email ) ){

			$reset_token = uniqid();
			
			$user = $this->users_model->get_user_by_email( $email );

			if( isset( $user )  && $this->users_meta_model->update_user_meta($user->id, 'password_reset_token', $reset_token) ) {

				$this->load->library('email', $this->config->item('smtp_config'));

				$this->email->set_newline("\r\n");

				$this->email->from($this->config->item('email_from'), $this->config->item("email_from_title"));
				$this->email->to($email);

				$this->email->subject('Password Reset');

				$this->email->message('Reset Token : '. $reset_token);

				if($this->email->send()) {
		            return $this->response(['message' => $this->message_email_send]);
		        } else {
		            return $this->response(['message' => $this->message_email_not_send], API_Controller::HTTP_BAD_REQUEST);
		        }

		    } else {
		    	return $this->response(['message' => "User not exits"], API_Controller::HTTP_UNAUTHORIZED);
		    }
		}
		
		return $this->response(['message' => "Email not valid"], API_Controller::HTTP_UNAUTHORIZED);
		
	}

	/**
	 * @api {put} /user/reset Reset Users password 
	 * @apiName Reset Password
	 * @apiGroup User
	 * @apiParam {String} email Users unique ID.
	 * @apiSuccess {Object} Return User object.
	 */

	public function reset_password() {
		$input_data = json_decode(trim(file_get_contents('php://input')), true);

		$this->valid_input_keys = array('email', 'reset_token', 'new_password');
		if(! $this->validate_inputs($input_data)){
			return $this->response(["message" =>API_Controller::MESSAGE_INVALID_DATAFORMAT], API_Controller::HTTP_FORBIDDEN);
		}

		$input_data = (object) $input_data;
		
		if(!$input_data->email || !$input_data->reset_token  || !$input_data->new_password) {
			return $this->response(["message" =>API_Controller::MESSAGE_INVALID_DATAFORMAT], API_Controller::HTTP_FORBIDDEN);
		}

		$email = $input_data->email;

		if(isset($email) and !valid_email($email)){
			return $this->response(["message" =>$this->message_email_not_valid], API_Controller::HTTP_FORBIDDEN);	
		}

		$user = $this->users_model->get_user_by_email($input_data->email);
		
		if( $user ){
			$reset_token = $this->users_meta_model->get_user_meta($user->id, 'password_reset_token');
			
			$new_password = $this->encrypt->encode( $input_data->new_password );
			if( $reset_token == $input_data->reset_token){
				$update_data = array(
					"data" => array('password'=> $new_password)
				);

				$this->db->trans_start();
				
				$this->users_model->update_user($user->id, (object)$update_data);
				$this->users_meta_model->delete_user_meta($user->id, 'password_reset_token');

				$new_token = generate_token($user->id);
				$this->users_meta_model->update_user_meta($user->id, 'auth_token', $new_token);
				 
				$this->db->trans_complete(); // transaction complete status

				return $this->response(['message' => $this->message_password_reset]);
			}

			return $this->response(['message' => $this->message_token_not_valid], API_Controller::HTTP_FORBIDDEN);
		}

		return $this->response(['message' => $this->message_user_not_found], API_Controller::HTTP_FORBIDDEN);
	}

	/**
	 * @api {put} /user/url/ Get User Unique Url  
	 * @apiName Get Unique User
	 * @apiGroup User
	 * @apiSuccess {String} Message text (Success and error)
	 */

	public function get_unique_url(){
		if( !$this->is_valid_user ){
			return $this->invalid_response();
		}

		// $url = $this->users_meta_model->get_user_meta($this->current_user->id, 'user_unique_url');
		// $url = site_url($this->config->item('activity_url').'/'.$url);

		$url = $this->_unique_url( $this->current_user->id );
		$data['data'] = array('url' => $url);
		$data['message'] = "Success";
		return $this->response($data);

	}

	public function _unique_url( $user_id ) {
		$url = $this->users_meta_model->get_user_meta( $user_id, 'user_unique_url');
		return $url = site_url($this->config->item('activity_url').'/'.$url);
	}

	public function _get_stripe_info( $user_id ){
		$this->load->model( 'stripe_model' );
		return $this->stripe_model->get_stripe_info( $user_id );
	}

	public function _get_stripe_customer_id( $user_id ) {
		$this->load->model( 'stripe_model' );
		$stripe_customer = $this->stripe_model->getByUser( $user_id );
		
		if( $stripe_customer ){
			return $stripe_customer->stripe_id;
		}

		return false;
	}

	/**
	 * @api {put} /user/url/  Update User Unique Url  
	 * @apiName Update Unique User
	 * @apiGroup User
	 * @apiSuccess {String} Message text (Success and error)
	 */
	public function update_unique_url(){
		if( !$this->is_valid_user ){
			return $this->invalid_response();
		}

		$url = get_unique_string();
		$update = $this->users_meta_model->update_user_meta($this->current_user->id, 'user_unique_url', $url);
		
		$url = site_url($this->config->item('activity_url').'/'.$url);
		
		if( $update ) {
			$data['data'] = array('url' => $url);
			$data['message'] = $this->message_update;
			return $this->response($data);	
		}

		return $this->response(['message' => $this->message_not_update], API_Controller::HTTP_BAD_REQUEST);
	}

	/**
	 * @api {post} /user/all-fortnox-account/  Get All accounts from fortnox  
	 * @apiName Get all Accounts
	 * @apiGroup User
	 * @apiSuccess {String} Message text (Success and error)
	 */
	public function get_all_fortnox_accoutns( $page=1 ) {
		if( !$this->is_valid_user ){
			return $this->invalid_response();
		}

		$user = $this->current_user;
		
		$this->load->library('fortnox/fortnox_accounts', 
			array(
				'client_secret'	=> $user->meta_data->fortnox_client_secret,
	 			'access_token' 	=> $user->meta_data->fortnox_token 
 			)
		);

		//update user_meta account_process is running
		$this->users_meta_model->update_user_meta( $this->current_user->id, 'fortnox_process', true );
			

		$accounts = $this->fortnox_accounts->get('?page='.$page, 1);

		if( $accounts->status_code != 200 ){
			$this->users_meta_model->update_user_meta( $this->current_user->id, 'fortnox_process', false );
			return $this->response( json_decode( $accounts->body ), 401 );
		}
		
		$accounts = json_decode( $accounts->body );

		if( !isset($accounts->MetaInformation)){
			return $this->response(['message' => "Complete"]);
		}

		$meta = (array) $accounts->MetaInformation;
		$total_page = intval( $meta['@TotalPages'] );
		$current_page = intval( $meta['@CurrentPage'] );
		log_message('error', 'Account created '. $current_page);

		 if ($current_page > $total_page) { // our base case
		     return 1;
		  }
		  else {

		  	if( !$accounts->Accounts) return 1;

			foreach($accounts->Accounts as $account ){
				$account_details = $this->fortnox_accounts->get($account->Number);
				$account_details = json_decode( $account_details->body );
				// print_r($accounts);
				
				$account_create_args = array(
					'user_id' 				=> $user->id, 
					'fortnox_account_no' 	=> $account->Number,
					'data'					=> json_encode($account_details),
					'vat_code'				=> $account_details->Account->VATCode,
					'vat'					=> $this->fortnox_accounts->get_fortnox_vat_percent( $account_details->Account->VATCode )
				);

				// print_r($account_create_args);

				$this->accounts_model->create( $account_create_args );
			}

			if( $current_page + 1 > $total_page ){
				$this->users_meta_model->update_user_meta( $this->current_user->id, 'fortnox_process', false );
				return $this->response(['message' => "All Complete, page ". $current_page ]);
			}

			return $this->get_all_fortnox_accoutns( $current_page + 1 ); // <--calling itself.
		  }

		exit;
	}

	/**
	 * @api {post} /user/get-fortnox-token  Get Fortnox API token and save to user meta  
	 * @apiName Get fortnox API token
	 * @apiGroup User
	 * @apiSuccess {String} Message text (Success and error)
	 */
	public function get_fortnox_token(){
		if( !$this->is_valid_user ){
			return $this->invalid_response();
		}

		$post_data = $input_data = json_decode(trim(file_get_contents('php://input')), true);
		
		$this->valid_input_keys = array('authorization_code', 'client_secret');

		if( empty( $post_data ) || !$this->validate_inputs( $post_data ) ) {
			return $this->response(["message" =>API_Controller::MESSAGE_INVALID_DATAFORMAT], API_Controller::HTTP_FORBIDDEN);
		}

		$this->load->library('fortnox/fortnox_order');
		$response = $this->fortnox_order->get_access_token( $post_data['client_secret'], $post_data['authorization_code'] );
		$token_body = json_decode( $response->body );

		if( $response->status_code == 200 ){
			$data['message'] = "Authorization key is generated successfully!";
			$data['data'] = $token_body->Authorization->AccessToken;

			$update = $this->users_meta_model->update_user_meta( $this->current_user->id, 'fortnox_token', $token_body->Authorization->AccessToken );
			$this->users_meta_model->update_user_meta( $this->current_user->id, 'fortnox_client_secret', $post_data['client_secret'] );
			$this->users_meta_model->update_user_meta( $this->current_user->id, 'fortnox_authorization_token', $post_data['authorization_code'] );
				
			if( $update ){
				return $this->response($data);
			}

		}

		$data['message'] = "Error: Generating authorization code";
		if( isset( $token_body->ErrorInformation->Message )){
			$data['message'] = $token_body->ErrorInformation->Message;
		}

		$data['data'] = $token_body;

		return $this->response($data, 401);
	}
}
