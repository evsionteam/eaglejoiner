<?php

defined('BASEPATH') OR exit('No direct script access allowed');
set_time_limit(0);

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 */

class Activity extends API_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->model("users_meta_model");
		$this->load->model("activity_model");
		$this->load->model("customers_model");
		$this->load->model("items_model");
		$this->load->model("orders_model");
		$this->load->model("accounts_model");
	}

	/**
	 * @api {post} /activity/:user_unique_url User Activity Manager
	 * @apiName Activity Manager
	 * @apiGroup Activity
	 * @apiDescription Activity monitor (create, update, update, delete)
	 * @apiSuccess {Object} Return User object.
	*/
	public function activity(){

		$user_url = $this->uri->segment(3); // 1stsegment

		$user = $this->users_meta_model->get_user(array('meta_key'=>'user_unique_url', 'meta_value' =>$user_url));
		if( ! $user ) {

			log_message("error", 'user not found ' . $user_url );
			return $this->response(['message' => 'User not found!']);
		}

		// update user_meta no_of_activity by sql trigger
		//CREATE TRIGGER `update_user_activity_count` AFTER INSERT ON `users_meta` FOR EACH ROW update users_meta set no_of_activity = no_of_activity + 1 where user_id = NEW.user_id
		// $user->fortnox_client_secret = '21UtsQd5xW';
		// $user->fortnox_token = "d579ad34-3a9e-4dd4-b826-4a051e414c15";

		$user->fortnox_client_secret = $user->meta_data->fortnox_client_secret;
		$user->fortnox_token = $user->meta_data->fortnox_token;

		file_put_contents(__DIR__.'/abc.txt', print_r($user,true), FILE_APPEND);

		$json = @$_POST['data'];
		$event = @$_POST['event'];

		$data = json_decode($json,true);
	
		file_put_contents(__DIR__.'/abcd1.txt', print_r($_POST,true),  FILE_APPEND);
		$data = $this->parse($data[0]);
		

		// log_message('error', 'Hooks loaded');
		// log_message('error', print_r($_POST,true));

		$insert['data'] = $json;
		$insert['user_id'] = $user->id; 
		$insert['activity_type'] = $event;
		$insert['response_code'] = 200;

		if( !$user ) {
			$insert['user_id'] = 0; 
		}

		$this->insert( $insert );

		/**
		 * Order Update if status is changed :)
		 **/
		if( isset( $data->status->title )){
			if( $data->status->title == 'Levererad'){
				log_message('error', 'Hooks loaded Levererad status');
				// create invoice and accounting
				$this->order_actions( $user, $_POST, 'invoice' );
				return;
			}

			// Order cancel
			if( $data->status->title == 'Borttagen'){
				// create invoice and accounting
				$this->order_actions( $user, $_POST, "cancel" );
				return;
			}
		}

		
		/**
		 * Prepare data for fortnox
		 **/
		switch($event){

			case 'orderNew':
				$this->new_order($user, $_POST);
				break;

			case 'orderUpdate':
				$this->order_update($user, $_POST);
				break;

			case 'productUpdate':
				// $this->ProductUpdate($user, $_POST);
				# code...
				break;
			
			default:
				print "Nothing";
		}
	}

	/**
	 * insert into activity table
	 */
	public function insert( $args ){
		
		return $this->activity_model->insert($args);
		
		// echo $this->db->last_query();
	}

	/**
	 * prepare_data for  activity table
	 */
	
	public function prepare_data( $user, $post ){
		$data = json_decode( $post['data'], true );
		
		$order_details = $this->parse( $data[0] );

		$customer_no = $this->get_customer_no( $user, $order_details );
		$hooks_data = array(
			'CustomerNumber'			=> $customer_no,
			'Freight'					=> floatval( $order_details->shipping )
		);

		if( isset( $order_details->market_reference ) ){
			$hooks_data['ExternalInvoiceReference1'] = $order_details->market_reference;
		}

		if ( isset( $order_details->market ) ) {
			$hooks_data['ExternalInvoiceReference2'] = $this->get_market_name( $order_details->market );
		}

		if( isset( $order_details->rows ) ){
			$order_items = $this->get_order_items( $user, $order_details->rows );
			$hooks_data['OrderRows'] = $order_items;
		}

		// prepare order data for fortnox
		return $hooks_data;

	}

	public function get_market_name( $market_id = null ){
		switch ( intval( $market_id ) ) {
			case 1:
				$market = "Tradera";
				break;
			case 5:
				$market = "SelloShop";
				break;
			case 6:
				$market = "Fyndiq";
				break;
			case 9:
				$market = "Magento";
				break;
			case 10:
				$market = "CDON";
				break;
			case 11:
				$market = "Prestashop";
				break;
			case 12:
				$market = "WooCommerce";
				break;
			case 13:
				$market = "Amazon Europe";
				break;
			default:
				$market = "";
				break;
		}

		return $market;
	}

	public function prepare_data_for_invoice( $user, $post, $invoice_type ){
		$data = json_decode( $post['data'], true );
		
		$order_details = $this->parse( $data[0] );
		$customer_no = $this->get_customer_no( $user, $order_details );

		$order_args = array( 'sello_order_id' => $order_details->order_id );
		$get_fortnox_order_id = $this->orders_model->get_fortnox_order_id( $user->id, $order_args);

		$hooks_data = array(
			'CustomerNumber'	=> $customer_no,
			'Freight'			=> floatval( $order_details->shipping ),
			'InvoiceType'       => "INVOICE"
		);

		if( $invoice_type ) {
			$hooks_data['CreditInvoiceReference'] = $get_fortnox_order_id;
		}

		if( isset( $order_details->rows ) ){
			$order_items = $this->get_order_items( $user, $order_details->rows, 1 );
			$hooks_data['InvoiceRows'] = $order_items;
		}

		// prepare order data for fortnox
		return $hooks_data;
	}
	
	/**
	 * new order
	 */
	public function new_order($user, $post){
		$hooks_data = $this->prepare_data( $user, $post );

		$data = json_decode( $post['data'], true );
		$order_details = $this->parse( $data[0] );

		$this->load->library('fortnox/fortnox_order', 
			array(
					'client_secret'	=> $user->fortnox_client_secret,
		 			'access_token' 	=> $user->fortnox_token 
	 			)
			);

		$result = $this->fortnox_order->create( $hooks_data );
	
		$insert['data'] = $result->body;
		$insert['user_id'] = $user->id; 
		$insert['activity_type'] = $post['event'];
		$insert['response_code'] = $result->status_code;

		if( !$result->success) {
			log_message('error', 'fortnox new_order fail to create');
			$inserted_data 				= array('previous' => $post, 'request_error' => $result->body);
			$insert['data'] 			= json_encode( $inserted_data );
			$insert['activity_type'] 	= 'needToRun';
			
			$this->insert( $insert );

			return;
		}

		$fortnox_order_details = json_decode( $result->body, true );
		$fortnox_order_details = $this->parse( $fortnox_order_details );

		
		$this->insert( $insert );


		// prepare data for order table (evision)
		$sello_order_status = 'Ny';
		if( $order_details->status->title ) { 
				$sello_order_status = $order_details->status->title;
		}

		$args = array('user_id' 		=> $user->id, 
				'sello_order_id' 		=> $order_details->order_id, 
				'sello_order_status' 	=> $sello_order_status,
				'fortnox_order_id' 		=> $fortnox_order_details->Order->DocumentNumber,
				'order_details'			=>$result->body 
			);
		// save to order table
		$this->orders_model->create( $args );
	}

	public function create_invoice($user, $post, $invoice_type = FALSE ) {
		$hooks_data = $this->prepare_data_for_invoice( $user, $post, $invoice_type );

		$data = json_decode( $post['data'], true );
		$order_details = $this->parse( $data[0] );

		$this->load->library('fortnox/fortnox_invoice', 
			array(
					'client_secret'	=> $user->fortnox_client_secret,
		 			'access_token' 	=> $user->fortnox_token 
	 			)
			);

		$result = $this->fortnox_invoice->create( $hooks_data );

		log_message('error', 'creating... credit invoice');
		if( !$result->success ){
			log_message('error', print_r($hooks_data , true) );
			log_message('error', print_r($result , true) );
			return;
		}

		log_message('error', 'created credit invoice');
		
		// log_message('error', print_r(json_decode( $result->body ), true));

		$insert['data'] = $result->body;
		$insert['user_id'] = $user->id; 
		$insert['activity_type'] = 'creditInvoice';
		$insert['response_code'] = $result->status_code;

		$this->insert( $insert );

		return $result;
	}

	public function order_update( $user, $post ) {

		$hooks_data = $this->prepare_data( $user, $post );
		
		$data = json_decode( $post['data'], true );
		$order_details = $this->parse( $data[0] );

		
		$this->load->library('fortnox/fortnox_order', 
			array(
					'client_secret'	=> $user->fortnox_client_secret,
		 			'access_token' 	=> $user->fortnox_token 
	 			)
			);

		// order status update 
		if( $order_details->status->title ) {
			$update_order_status = $this->orders_model->update_by_sello_id( 
				$order_details->order_id, 
				array('sello_order_status'=> $order_details->status->title)
			);
		}
		// get fortnox order id to update 
		$order_args = array( 'sello_order_id' => $order_details->order_id );
		$get_fortnox_order_id = $this->orders_model->get_fortnox_order_id( $user->id, $order_args);
		$result = $this->fortnox_order->update( $get_fortnox_order_id, $hooks_data );
		
		if( !$result->success) {
			log_message('error', 'fortnox fortnox_order update');
			log_message('error', print_r($result, true));
		}

		// file_put_contents(__DIR__.'/abc.txt', print_r($hooks_data,true), FILE_APPEND);
		$insert['data'] = $result->body;
		$insert['user_id'] = $user->id; 
		$insert['activity_type'] = $post['event'];
		$insert['response_code'] = $result->status_code;

		$this->insert( $insert );

	}

	private function parse(array $arr, stdClass $parent = null) {
	   
	    foreach ($arr as $key => $val) {
	        if (is_array($val)) {
	            $parent->$key = $this->parse($val, new stdClass);
	        } else {
	            $parent->$key = $val;
	        }
	    }

	    return $parent;
	}


	public function get_customer_no($user, $order_details ){

		$sello_customer = $order_details->customer;
		$email = $sello_customer->contact->email;
		
		$this->load->library('fortnox/fortnox_customer', 
			array(
					'client_secret'	=> $user->fortnox_client_secret,
		 			'access_token' 	=> $user->fortnox_token 
	 			)
		);

		// check user alredy exits or not 
		$evision_customer = $this->customers_model->get_fortnox_customer_id( $user->id, array( 'email' => $email ) );
		$customer_no = $evision_customer;
		
		// if not customer on database save and create to fortnox customer
		if( !$evision_customer ) {

			$fortnox_cus_args = array(
					'Name'			=> $sello_customer->address->first_name .' ' . $sello_customer->address->last_name, 
					'Address1'		=> $sello_customer->address->address, 
					'Address2'		=> $sello_customer->address->address2, 
					'City'			=> $sello_customer->address->city, 
					'Currency'		=> $order_details->currency, 
					'CountryCode'	=> $sello_customer->address->country->code,
					'Email'			=> $email
				);

			//create custoemr to fortnox
			$response = $this->fortnox_customer->create($fortnox_cus_args);
			$fortnox_customer = json_decode($response->body);

			$customer_no = $fortnox_customer->Customer->CustomerNumber;

			$evision_customer_args = array(
					'user_id'				=> $user->id,
					'sello_customer_id' 	=> $order_details->customer_id,
					'fortnox_customer_id'	=> $customer_no,
					'name'					=> $fortnox_cus_args['Name'],
					'email'					=> $fortnox_cus_args['Email']

				);

			$this->customers_model->create($evision_customer_args);
			
		}

		return $customer_no;
	}

	public function get_order_items( $user, $order_rows, $invoice = FALSE ) {

		$this->load->library('fortnox/fortnox_item', 
			array(
					'client_secret'	=> $user->fortnox_client_secret,
		 			'access_token' 	=> $user->fortnox_token 
	 			)
			);

		$order_items = array();
		
		foreach($order_rows as $row){
			$order_id = $row->product_id;
			
			if( !$order_id ) {
				$order_id = $row->id;
			}

			$evision_item = $this->items_model->get_row($user->id, array('sello_item_id'=> $order_id ));
			
			$fortnox_account_no = $this->get_fortnox_account_no( $user, intval($row->tax) );

			// $purchase_price = floatval( $row->price ) - ( floatval( $row->price ) * floatval( $row->vat ) ) / 100;
			$purchase_price = floatval( ( ( floatval( $row->quantity ) *  floatval( $row->price ) )  - floatval( $row->vat ) ) / floatval($row->quantity) );

			if( !$evision_item ){
				// create itesm to fortnox and evision
				$fortnox_item_args = array(
						'Description' 		=> $row->title,
						'PurchasePrice' 	=> $purchase_price,
						'Note'				=> $row->reference,
						'QuantityInStock'	=> '100'			
					);

				$fortnox_response = $this->fortnox_item->create( $fortnox_item_args );
				// save to activity table
				$insert['data'] = $fortnox_response->body;
				$insert['user_id'] = $user->id; 
				$insert['activity_type'] = 'itemCreate';
				$insert['response_code'] = $fortnox_response->status_code;
				$this->insert( $insert );

				$fortnox_item = json_decode( $fortnox_response->body );

				if( !$fortnox_response->success) {
					log_message('error', 'fortnox item crated');
					log_message('error', print_r($fortnox_response, true));
				}

				//create price current items
				$response_price = $this->fortnox_item->create_price( $fortnox_item->Article->ArticleNumber, $purchase_price );
				
				// save to activity table
				$insert['data'] = $response_price->body;
				$insert['user_id'] = $user->id; 
				$insert['activity_type'] = 'priceCreate';
				$insert['response_code'] = $response_price->status_code;
				$this->insert( $insert );

				if( !$response_price->success) {
					log_message('error', 'fortnox price crated');
					log_message('error', print_r($response_price, true));
				}

				// print_r($fortnox_item);
				//save log to evision activity and create items on evision 
				$this->items_model->insert(array(
						'user_id'			=> $user->id, 
						'sello_item_id'		=> $order_id,
						'fortnox_item_id'	=> $fortnox_item->Article->ArticleNumber,
						'item_name'			=> $row->title
					));

				// prepare data for order items rows
				$item = array(
					"ArticleNumber" 		=> $fortnox_item->Article->ArticleNumber,
					"DeliveredQuantity"		=> $row->quantity,
					"Description"			=> $row->title,
					"OrderedQuantity"		=> $row->quantity,
					"AccountNumber"			=> $fortnox_account_no
				);

				if( $invoice ){
					unset($item['OrderedQuantity']);
				}

				$order_items[] = $item;

			}else{
				// prepare data for order items rows
				$item = array(
					"ArticleNumber" 		=> $evision_item->fortnox_item_id,
					"DeliveredQuantity"		=> $row->quantity,
					"Description"			=> $row->title,
					"OrderedQuantity"		=> $row->quantity,
					"AccountNumber"			=> $fortnox_account_no
				);
				if( $invoice ){
					unset($item['OrderedQuantity']);
				}
				$order_items[] = $item;

			}
		}

		return $order_items;
	}


	public function get_fortnox_account_no( $user, $vat ) {
		$fortnox_account_no = $this->accounts_model->get_row( $user->id, array('vat' =>$vat, "vat_code !="=> NULL) );
		
		$fortnox_account_no = $fortnox_account_no->fortnox_account_no;

		if( empty($fortnox_account_no) ) {

			$account_response = $this->create_fortnox_account( $user, $vat );

			$data = (object) json_decode( $account_response, true );
			
			$data = $this->parse($data);
			$data->vat = $vat;

			$this->create_account_record( $user, $data );

			$fortnox_account_no = $data->Account->Number;
		}


		return $fortnox_account_no;
	}

	public function create_fortnox_account( $user, $vat ) {

		$this->load->library('fortnox/fortnox_accounts', 
			array(
					'client_secret'	=> $user->fortnox_client_secret,
		 			'access_token' 	=> $user->fortnox_token 
	 			)
			);

		$account_args = array(
				"Number"		=> intval( $this->accounts_model->get_last_account_no( $user->id ) ) + 1,
				"VATCode"		=> $this->fortnox_accounts->get_fortnox_vat_code( $vat ),
				"Description"	=> $vat,
				"Active"		=> 1
			);	

		$response = $this->fortnox_accounts->create( $account_args );
		// save to activity table
		$insert['data'] = $response->body;
		$insert['user_id'] = $user->id; 
		$insert['activity_type'] = 'accountCreate';
		$insert['response_code'] = $response->status_code;
		$this->insert( $insert );

		
		if( !$response->success) {
			log_message('error', 'fortnox account crated');
			log_message('error', print_r($response->body, true));
		}
		

		
		return $response->body;
	  	
	}

	public function create_account_record( $user, $data ) {
		
		$account_create_args = array(
			'user_id' 				=> $user->id, 
			'fortnox_account_no' 	=> $data->Account->Number,
			'data'					=> json_encode( $data ),
			'vat'					=> $data->vat,
			'vat_code'				=> $this->fortnox_accounts->get_fortnox_vat_code( intval( $data->vat ) ),
		);
		
		return $this->accounts_model->insert( $account_create_args );

	}

	/**
	 *	Order action for order cancel and create invoice
	 *  This funciton create inoice and bookkeep the invoice
	 **/
	public function order_actions( $user, $post, $action="invoice" ) {
		$data = json_decode( $post['data'], true );
		$order_details = $this->parse( $data[0] );
		log_message('error', 'fortnox order_actions');

		$this->load->library('fortnox/fortnox_order', 
			array(
					'client_secret'	=> $user->fortnox_client_secret,
		 			'access_token' 	=> $user->fortnox_token 
	 			)
			);

		$order_args = array(
				'sello_order_id' => $order_details->order_id
			);
		
		$fortnox_order_row = $this->orders_model->get_order_row( $user->id, $order_args);
		$fortnox_order_id = $fortnox_order_row->fortnox_order_id;
		$sello_order_status = $fortnox_order_row->sello_order_status;
		
		log_message('error', 'send to invoice => order _id  '. $fortnox_order_id );

		if( $action == 'invoice' ) {
			log_message('error', 'inside invoice');

			$update_order_status = $this->orders_model->update_by_sello_id( 
				$order_details->order_id, 
				array('sello_order_status'=> $order_details->status->title)
			);
			
			log_message('error', 'update_order_status ' .$update_order_status);
			
			$created = $this->fortnox_order->create_invoice( $fortnox_order_id );

			log_message("error", 'Invoice created ' );
			// log_message("error", print_r( $created , true));
			
			if( $created ->success ) {
				log_message("error", 'Invoice created success' );
				
				$order_details = json_decode( $created->body );
				// log_message("error", print_r( $order_details , true));

				$invoice_no = $order_details->Order->InvoiceReference;
				$this->bookkeep_invoice( $this->fortnox_order, $invoice_no );

				log_message("error", 'bookkeep_invoice' );
			}
			
			if( !$created->success ) {
				log_message('error', 'fortnox invoice created');
				// log_message('error', print_r($created->body, true));

				// now create another invoice
				if( isset( $order_details->status->title ) && $sello_order_status == "Borttagen" ) {
					log_message("error", 'new invoice creating....');
					
					$invoice_details= $this->create_invoice( $user, $post );
					
					log_message("error", 'credit invoice created');
					// log_message("error", print_r( $invoice_details ) );

					$invoice_details = json_decode( $invoice_details->body );

					$invoice_no = $invoice_details->Invoice->DocumentNumber;
					
					log_message("error", 'invoice_details of invoice '. $invoice_no );
					
					$this->bookkeep_invoice( $this->fortnox_order, $invoice_no );
				}

			}

			return;
		}

		if( $action == 'cancel' ) {
			$response = $this->fortnox_order->order_cancel( $fortnox_order_id );
	
			$update_order_status = $this->orders_model->update_by_sello_id( 
				$order_details->order_id, 
				array('sello_order_status'=> $order_details->status->title)
			);
			
			if( $sello_order_status == 'Levererad'){
				// generate credit invoice
				log_message("error", 'credit invoice creating....');
					
				$invoice_details = $this->create_invoice($user, $post, 'credit');
				
				log_message("error", 'credit invoice created');
				// log_message("error", print_r( $invoice_details ) );

				$invoice_details = json_decode( $invoice_details->body );

				$invoice_no = $invoice_details->Invoice->DocumentNumber;
				
				log_message("error", 'invoice_details of invoice '. $invoice_no );
				
				$this->bookkeep_invoice( $this->fortnox_order, $invoice_no );

			}

			// save to activity table
			$insert['data'] = $response->body;
			$insert['user_id'] = $user->id; 
			$insert['activity_type'] = 'orderCancel';
			$insert['response_code'] = $response->status_code;
			$this->insert( $insert );

			
			if( !$response->success) {
				log_message('error', 'fortnox order cancel');
				// log_message('error', print_r($response->body, true));
			}

			return;
		}

	}

	/**
	 * Book Keep invoice by  invoice number
	 **/
	public function bookkeep_invoice( $library, $invoice_no ) {
		log_message("error", 'inside bookkeep');
		if( $invoice_no ){
			log_message("error", "BookKeep invoice Create ". $invoice_no );
			
			$bookkeep = $library->bookkeep( $invoice_no );
			if( $bookkeep->success )
				log_message("error", "bookkeep done ");
			else
				log_message("error", print_r($bookkeep, true) );
		}else{
			log_message("error", "BookKeep invoice InvoiceReference not found");
			log_message('error', print_r($invoice_no, true));
		}		
	}


	/**
	 * @api {get} /activity:(page_number) Get All activity of current user by pagination 
	 * @apiName Activity list
	 * @apiGroup Activity
	 * @apiSuccess {Object} Message text (Success and error)
	 */
	public function get(){
		$this->validate();

		if( !$this->is_valid_user ){
			return $this->response(['error'=> API_Controller::MESSAGE_INVALID_USER ], API_Controller::HTTP_FORBIDDEN );
		}

		$this->load->helper("url");
		$this->load->library("pagination");

		$config = array();
        $config["total_rows"] = $this->activity_model->record_count( $this->current_user->id);
        $config["per_page"] = 15;
        $config["uri_segment"] = 2;

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;

        $offset = $page == 0? 0: ($page-1)*$config["per_page"];

        $activity = $this->activity_model->fetch( $this->current_user->id, $config["per_page"], $offset);
        $data['data'] = array();
		$data['status'] = API_Controller::HTTP_OK;
		$data['filter'] = array(
								'activity_type' => $this->activity_model->distinct_field(array('user_id' => $this->current_user->id), 'activity_type'),
								'response_code' => $this->activity_model->distinct_field(array('user_id' => $this->current_user->id), 'response_code'),
							 );
		
		
		if( $activity ){
			$data['data'] 		= $activity;
			$data['total_rows'] = $config["total_rows"];
			$data['per_page'] 	= $config["per_page"]; 
			$data['message'] 	= "Success";

			return $this->response( $data );
		}

		$data['message'] = "Record not found";
		$data['total_rows'] = 0 ;
		return $this->response($data);

	}

	/**
	 * @api {post} /activity Delete single or multiple row of activity table 
	 * @apiName Delete Activity Row
	 * @apiGroup Activity
	 * @params id: int / array (1, [1,2,3 ] )
	 * @apiSuccess {String} Message text (Success and error)
	 */
	public function delete() {
		$this->validate();

		if( !$this->is_valid_user ){
			return $this->response(['error'=> API_Controller::MESSAGE_INVALID_USER ], API_Controller::HTTP_FORBIDDEN );
		}

		$input_data = json_decode(trim(file_get_contents('php://input')), true);

		$this->valid_input_keys = array('id');
		if(!$this->validate_inputs( $input_data )){
			return $this->response(["message" =>API_Controller::MESSAGE_INVALID_DATAFORMAT], API_Controller::HTTP_FORBIDDEN);
		}
		
		$data['message'] = "Error";
		
		if( $this->activity_model->delete( $this->current_user->id, $input_data['id'] ) ){
			$data['message'] = "Successfully Deleted";
		}
	
		return $this->response($data, API_Controller::HTTP_OK );
	}

	public function delete_all(){
		$this->validate();

		if( !$this->is_valid_user ){
			return $this->response(['error'=> API_Controller::MESSAGE_INVALID_USER ], API_Controller::HTTP_FORBIDDEN );
		}

		if( $this->activity_model->delete( $this->current_user->id ) ) {
			$data['message'] = "Successfully Deleted";
			$data['status'] = API_Controller::HTTP_OK;

			return $this->response($data);		
		}
		
		$data['message'] = "Not deleted !";
		$data['status'] = API_Controller::HTTP_FORBIDDEN;

		return $this->response( $data );
	}
	
	/**
	 * @api {purge} /activity truncate activity table 
	 * @apiName Truncate Activity Table
	 * @apiGroup Activity
	 * @apiSuccess {String} Message text (Success and error)
	 */
	public function truncate() {
		$this->validate();

		if( !$this->is_valid_user ){
			return $this->response(['error'=> API_Controller::MESSAGE_INVALID_USER ], API_Controller::HTTP_FORBIDDEN );
		}

		$data['message'] = "Error";
		
		if( $this->activity_model->truncate() ){
			$data['message'] = "Successfully truncate table";
		}

		return $this->response($data);
	}

	/**
	 * @api {post} /activity/(:page_num) filter activity type 
	 * @apiName Activity Filter 
	 * @apiGroup Activity
	 * @apiParams {"filter": {"activity_type": "orderUpdate",  "response_code": 200, "created_date": "2016-06-20" }}
	 * @apiSuccess {Object} Message text (Success and error)
	 */
	public function filter( $page = 0 ){
		$this->validate();

		if( !$this->is_valid_user ){
			return $this->invalid_response( );
		}

		$input_data = json_decode(trim(file_get_contents('php://input')), true);
		
		$this->valid_input_keys = array('filter');
		if(!$this->validate_inputs( $input_data )){
			return $this->response(["message" =>API_Controller::MESSAGE_INVALID_DATAFORMAT], API_Controller::HTTP_FORBIDDEN);
		}



		$this->load->helper("url");
		$this->load->library("pagination");

		$config = array();
        $config["per_page"] = 15;
       
        $this->pagination->initialize($config);

        // $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $offset = $page == 0 ? 0: ($page-1)*$config["per_page"];

		
		$filter = $input_data['filter'];

		$this->valid_input_keys = array('activity_type', 'response_code', 'gt_date', 'lt_date');
		if(!$this->validate_inputs( $filter )){
			$message = "only filter by activity_type, response_code, gt_date, lt_date and user_id";
			$r_data['message']	= $message;
			$r_data['status']	= API_Controller::HTTP_FORBIDDEN;

			return $this->response( $r_data );
		}

		if( is_array( $filter ) ) {
			$filter['user_id'] = $this->current_user->id;
		}

		if( isset($filter['gt_date'])){
			$filter['created_date <='] = $filter['gt_date'];
			unset($filter['gt_date']);
		}

		if( isset($filter['lt_date'])){
			$filter['created_date >='] = $filter['lt_date'];
			unset($filter['lt_date']);
		}

		$activity = $this->activity_model->filter( $filter, $config["per_page"], $offset );
		
		$config["total_rows"] = $this->activity_model->record_count( $this->current_user->id, $filter );
        
		$data['data'] = array();
		$data['filter'] = array(
								'activity_type' => $this->activity_model->distinct_field(array('user_id' => $this->current_user->id), 'activity_type'),
								'response_code' => $this->activity_model->distinct_field(array('user_id' => $this->current_user->id), 'response_code'),
							 );
		$data['status']		 = API_Controller::HTTP_OK;

		if( $activity ){
			$data['data'] 		= $activity;
			$data['total_rows'] = $config["total_rows"];
			$data['per_page'] 	= $config["per_page"]; 
			$data['message'] 	= "Success";

			return $this->response( $data );
		}

		$data['message'] = "Record not found";
		$data['total_rows'] = 0;
		return $this->response($data, API_Controller::HTTP_OK);

	}

	/**
	 * @api {get} /activity filter activity by users Id 
	 * @apiName Activity Filter by user id
	 * @apiGroup Activity
	 * @apiSuccess {Object} Message text (Success and error)
	 */
	public function export(){
		$this->validate();

		if( !$this->is_valid_user ){
			return $this->invalid_response( );
		}

		$activity = $this->activity_model->get_by_user( $this->current_user->id  );
		 // echo $this->db->last_query();
		if( $activity ){
			$data['data'] 		= $activity;
			$data['message'] 	= "Success";
			$data['status'] 	= API_Controller::HTTP_OK;

			return $this->response( $data );
		}

		$data['data'] 			= array();
		$data['message'] 		= "Record not found";
		$data['status_code'] 	= API_Controller::HTTP_OK;

		return $this->response( $data );

	}

}