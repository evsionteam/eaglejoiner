<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is stripe api class
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          EagleVision IT
 * @license         MIT
 **/

class Stripe_api extends API_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->library( 'stripe/stripe' );
		$this->config->load('stripe');
		if( $this->config->item('stripe_test_mode')){
			$stripe_api_key = $this->config->item('stripe_key_test_secret');
		}else {
			$stripe_api_key = $this->config->item('stripe_key_live_secret');
		}
		
		\Stripe\Stripe::setApiKey( $stripe_api_key );

		/*
		$customer = \Stripe\Customer::retrieve("cus_90eZzVbRWWoV8B");
		// $customers = \Stripe\Customer::all(array("limit" => 3));

		echo '<pre>';
		$customer_array = $customer->__toArray(true);
		print_r($customer_array);
		*/
		$this->load->model( 'stripe_model' );

		$this->validate();
	}


	public function webhook_receiver(){
		$json = file_get_contents('php://input');
		$action = json_decode($json, true);

		// file_put_contents(__DIR__.'/stripe.txt', print_r($action,true), FILE_APPEND);

		$data 					= $action['data']['object'];
		$customer_id 			= $data['customer'];
		$subscription_id 		= $data['id'];
		$canceled_at 			= $data['canceled_at'];
		$current_period_end 	= $data['current_period_end'];
		$cancel_at_period_end 	= $data['cancel_at_period_end'];

		$update_args = array(
			'subscription_id' 					=> $subscription_id,
			'subscription_canceled_at' 			=> $canceled_at,
			'subscription_current_period_end' 	=> $current_period_end,
			'subscription_cancel_at_period_end' => $cancel_at_period_end, 
			'subscription_deleted'				=> false
		);

		if( $action['type'] == 'customer.subscription.updated' || $action['type'] == 'customer.subscription.created' ){
			
			$this->stripe_model->update($customer_id , $update_args );

		}

		if( $action['type'] == 'customer.subscription.deleted'){
			$update_args['subscription_deleted'] = 1;
			$this->stripe_model->update($customer_id , $update_args );
		}


		if( $action['type'] == 'charge.succeeded'){
			$update_args['paid'] = 1;
			$this->stripe_model->update($customer_id , $update_args );
		}

		if( $action['type'] == 'charge.failed'){
			$update_args['paid'] = 0;
			$this->stripe_model->update($customer_id , $update_args );
		}


		return $this->response("done");
	}

	public function get_customer( ) {
		if( !$this->is_valid_user ){
			return $this->response(['error'=> API_Controller::MESSAGE_INVALID_USER ], API_Controller::HTTP_FORBIDDEN );
		}
		
		$id = $this->uri->segment(3); // 1stsegment

		$data['message'] = 'Success';
		
		if( $id ){
			// $customer = json_decode( $this->stripe->customer_info( $id ) );
			try {
				$customer = \Stripe\Customer::retrieve( $id );
				$customer = $customer->__toArray(true);
			}
			catch (Exception $e) {
				$body = $e->getJsonBody();
  				$err  = $body['error'];

				$data['message'] = $err['message'];
				$data['data'] = $body;
				return $this->response( $data, API_Controller::HTTP_NOT_FOUND );
			}

		}else {
			$customer = \Stripe\Customer::all();
			$customer = $customer->__toArray(true);
		}

		$data['data'] = $customer;

		return $this->response( $data );
	}

	public function create_customer( ) {
		
		if( !$this->is_valid_user ){
			return $this->response(['error'=> API_Controller::MESSAGE_INVALID_USER ], API_Controller::HTTP_FORBIDDEN );
		}

		// check stripe customer is already
		// $stripe_customer = $this->stripe_model->getByUser( $this->current_user->id );
		// if( $stripe_customer ){
		// 	$data['message'] = "Customer already subscribed";
		// 	$data['data'] = $stripe_customer;
		// 	return $this->response( $data );
		// }

		$input_data = json_decode(trim(file_get_contents('php://input')), true);
		
		$this->valid_input_keys = array('stripeToken', 'stripeTokenType', 'stripeEmail');

		if( !$this->validate_inputs($input_data)){
			return $this->response(["message" =>API_Controller::MESSAGE_INVALID_DATAFORMAT], API_Controller::HTTP_FORBIDDEN);
		}

		$input_data = (object) $input_data;
		
		$subscribe_plan = "sello2fortnox";
		/*
		if( isset( $input_data->stripePlan ) ) {
			$subscribe_plan = $input_data->stripePlan;
		}
		*/
		$stripe_args = array(
			  "description" => "Customer for {$input_data->stripeEmail}",
			  "source" 		=> $input_data->stripeToken, // obtained with Stripe.js
			  "plan"		=> $subscribe_plan,
			  "email"		=> $input_data->stripeEmail
			);

		try {
			$customer = \Stripe\Customer::create( $stripe_args );
			$customer = $customer->__toArray(true);
		}
		catch (Exception $e) {
			$body = $e->getJsonBody();
			$err  = $body['error'];

			$data['message'] 	= $err['message'];
			$data['data'] 		= $body;

			return $this->response( $data, API_Controller::HTTP_NOT_FOUND );
		}

		$data = array(
				'user_id' 		=> $this->current_user->id, 
				'stripe_id' 	=> $customer['id'], 
				'email'			=> $customer['email'],
				'currency'		=> $customer['currency'],
				'subscriptions'	=> json_encode( $customer['subscriptions']['data'] ),
				'response'		=> json_encode( $customer )
			);

		$insert = $this->stripe_model->insert( $data );

		$return_data['message'] = "Success";
		$return_data['data'] = $customer;

		return $this->response( $return_data );
	}

	public function delete_customer( ) {

	}

	/**
	 * @api {get} /stripe/card Stripe Card List
	 * @apiName Stripe Card List
	 * @apiGroup Stripe
	 *
	 * @apiSuccess {Object} Return User object.
	*/
	public function get_cards(){
		if( !$this->is_valid_user ){
			return $this->response(['error'=> API_Controller::MESSAGE_INVALID_USER ], API_Controller::HTTP_FORBIDDEN );
		}

		$customer_id = $this->uri->segment(3);
		
		try {
			$cards =  \Stripe\Customer::retrieve($customer_id)->sources->all(array(
  				"object" => "card"
			));

			$cards = $cards->__toArray(true);
		}
		catch (Exception $e) {
			$body = $e->getJsonBody();
			$err  = $body['error'];

			$data['message'] 	= $err['message'];
			$data['data'] 		= $body;
			$data['status']		= API_Controller::HTTP_NOT_FOUND;
			return $this->response( $data );
		}

		$data['message'] 	= "Success";
		$data['data'] 		= $cards ;
		$data['status']		= API_Controller::HTTP_OK;

		return $this->response( $data );
	}

	/**
	 * @api {post} /stripe/card Stripe Card Create
	 * @apiName Stripe Card Create
	 * @apiGroup Stripe
	 * 	$args = array(
	 *	     'number'    => '4242424242424242',
	 *	     'exp_month' => 10,
	 *	     'cvc'       => 314,
	 *	     'exp_year'  => 2020
	 *	  );
	 *	 
	 * $card_data = array('number', 'exp_month', 'cvc', 'exp_year')
	 *
	 * @apiSuccess {Object} Return Stripe object.
	*/
	public function create_card(){

		if( !$this->is_valid_user ){
			return $this->response(['error'=> API_Controller::MESSAGE_INVALID_USER ], API_Controller::HTTP_FORBIDDEN );
		}

		$input_data = json_decode(trim(file_get_contents('php://input')), true);
		
		$this->valid_input_keys = array('stripeCustomer', 'card_data' );

		if( !$this->validate_inputs($input_data)){
			return $this->response(["message" =>API_Controller::MESSAGE_INVALID_DATAFORMAT], API_Controller::HTTP_FORBIDDEN);
		}

		$card = $this->stripe->card_create( $input_data['stripeCustomer'], $input_data['card_data'] );

		$card = json_decode( $card ) ;

		if( isset( $card->error ) ) {
			$data['message'] 	= $card->error->message;
			$data['data'] 		= $card;
			$data['status']		= API_Controller::HTTP_FORBIDDEN;
			return $this->response($data );
		}

		$data['message'] 	= "Customer Card successfully created!";
		$data['data'] 		= $card;
		$data['status']		= API_Controller::HTTP_OK;

		return $this->response( $data );
	}

	/**
	 * @api {put} /stripe/card Stripe Card Update
	 * @apiName Stripe Card Update
	 * @apiGroup Stripe
	 * 	$args = array(
	 *	     'number'    => '4242424242424242',
	 *	     'exp_month' => 10,
	 *	     'cvc'       => 314,
	 *	     'exp_year'  => 2020
	 *	  );
	 * $card_data [ 'name'  => 'John Doe', 'address_line1' => 'Example Street 1']
	 *	 array('name', 'address_line1', 'address_line2', 'address_city', 'address_zip', 'address_state', 'address_country', 'exp_month', 'exp_year')
	 *
	 * @apiSuccess {Object} Return Stripe object.
	*/

	public function update_card() {
		if( !$this->is_valid_user ){
			return $this->response(['error'=> API_Controller::MESSAGE_INVALID_USER ], API_Controller::HTTP_FORBIDDEN );
		}

		$input_data = json_decode(trim(file_get_contents('php://input')), true);
		
		$this->valid_input_keys = array('stripeCustomer', 'stripeCard', 'card_data' );

		if( !$this->validate_inputs($input_data)){
			return $this->response(["message" =>API_Controller::MESSAGE_INVALID_DATAFORMAT], API_Controller::HTTP_FORBIDDEN);
		}

		$card = $this->stripe->card_update( $input_data['stripeCustomer'], $input_data['stripeCard'], $input_data['card_data'] );

		$card = json_decode( $card ) ;

		if( isset( $card->error ) ) {
			$data['message'] 	= $card->error->message;
			$data['data'] 		= $card;
			$data['status']		= API_Controller::HTTP_FORBIDDEN;
			return $this->response($data );
		}

		$data['message'] 	= "Customer Card successfully updated!";
		$data['data'] 		= $card;
		$data['status']		= API_Controller::HTTP_OK;

		return $this->response( $data );

	}

	/**
	 * @api {delete} /stripe/card Stripe Card Delete
	 * @apiName Stripe Card Delete
	 * @apiGroup Stripe
	 *
	 * @apiSuccess {Object} Return object.
	 */

	public function delete_card() {
		if( !$this->is_valid_user ){
			return $this->response(['error'=> API_Controller::MESSAGE_INVALID_USER ], API_Controller::HTTP_FORBIDDEN );
		}

		$customerId = $this->uri->segment(3);
		$cardId = $this->uri->segment(4);

		try {
			$card = \Stripe\Customer::retrieve( $customerId );
			$card = $card->sources->retrieve( $cardId )->delete();

			$card = $card->__toArray(true);
		}
		catch (Exception $e) {
			$body = $e->getJsonBody();
			$err  = $body['error'];

			$data['message'] 	= $err['message'];
			$data['data'] 		= $body;
			$data['status'] 	= API_Controller::HTTP_FORBIDDEN;

			return $this->response( $data );
		}

		$data['message'] 	= "Successfully Deleted";
		$data['data'] 		= $card ;
		$data['status'] 	= API_Controller::HTTP_OK;

		return $this->response( $data );
	}
	
	/**
	 * @api {get} /stripe/subscription Stripe Subscription list
	 * @apiName Stripe Subscription list
	 * @apiGroup Stripe
	 *
	 * @apiSuccess {Object} Return object.
	 */

	public function subscription_all( ){
		if( !$this->is_valid_user ){
			return $this->response(['error'=> API_Controller::MESSAGE_INVALID_USER ], API_Controller::HTTP_FORBIDDEN );
		}

		$customerId 	= $this->uri->segment(3);
		
		$subscription 	= $this->stripe->subscription_all( $customerId );

		$subscription  	= json_decode( $subscription ) ;

		if( isset( $subscription->error ) ) {
			$data['message'] 	= $subscription->error->message;
			$data['data'] 		= $subscription;

			return $this->response($data, API_Controller::HTTP_FORBIDDEN );
		}

		// sent subscription email
		$user = $this->current_user;
		$data = array('user' => $user->username);
		$email_template = $this->load->view('email-template/subscription.php', $data, TRUE);
		eagle_send_mail( $user->email, 'EagleJoiner: Subscription', $email_template );

		$data['message'] 	= "Success";
		$data['data'] 		= $subscription ;
		return $this->response( $data );
	}

	/**
	 * @api {post} /stripe/unsubscribe Stripe unsubscribe
	 * @apiName Stripe unsubscribe
	 * @apiGroup Stripe
	 *
	 * @apiSuccess {Object} Return object.
	 */
	public function unsubscribe() {

		if( !$this->is_valid_user ){
			return $this->response(['error'=> API_Controller::MESSAGE_INVALID_USER ], API_Controller::HTTP_FORBIDDEN );
		}

		$customerId = $this->uri->segment(3);
		
		$unsubscribe 	= $this->stripe->customer_unsubscribe($customerId);

		$unsubscribe 	= json_decode( $unsubscribe ) ;

		if( isset( $unsubscribe->error ) ) {
			$data['message'] 	= $unsubscribe->error->message;
			$data['data'] 		= $unsubscribe;
			$data['status']		= API_Controller::HTTP_FORBIDDEN;
			return $this->response( $data );
		}
		
		// send unsubscription email
		$user = $this->current_user;
		$data = array('user' => $user->username);
		$email_template = $this->load->view('email-template/unsubscription.php', $data, TRUE);
		eagle_send_mail( $user->email, 'EagleJoiner: Un Subscription', $email_template );


		$data['message'] 	= "Successfully unsubscribe";
		$data['data'] 		= $unsubscribe ;
		$data['status']		= API_Controller::HTTP_OK;

		return $this->response( $data );
	}

	/**
	 * @api {post} /stripe/update/subscription Stripe update subscription
	 * @apiName Stripe update subscription
	 * @apiGroup Stripe
	 *
	 * @apiSuccess {Object} Return object.
	 */
	public function again_subscription(){

		if( !$this->is_valid_user ){
			return $this->response(['error'=> API_Controller::MESSAGE_INVALID_USER ], API_Controller::HTTP_FORBIDDEN );
		}

		$input_data = json_decode(trim(file_get_contents('php://input')), true);
		
		$this->valid_input_keys = array('stripeSubscriptionId');

		if( !$this->validate_inputs($input_data)){
			$data['message'] 	= API_Controller::MESSAGE_INVALID_DATAFORMAT;
			$data['status']		= API_Controller::HTTP_FORBIDDEN;
			return $this->response( $data );
		}

		//check current user subscription id is valid or not
		$stripe_customer = $this->stripe_model->get_row_by_user( $this->current_user->id );

		if( $stripe_customer && $stripe_customer->subscription_id != $input_data['stripeSubscriptionId']){
			$data['message'] 	= "Invalid Subscription ID";
			$data['status']		= API_Controller::HTTP_FORBIDDEN;
			return $this->response( $data );
		}

		try{
			$subscription = \Stripe\Subscription::retrieve( $input_data['stripeSubscriptionId'] );
			$subscription->plan = "sello2fortnox";
			$subscription->save();
		}
		catch (Exception $e) {

			$body = $e->getJsonBody();
			$err  = $body['error'];

			$data['message'] 	= $err['message'];
			$data['data'] 		= $body;
			$data['status']		= API_Controller::HTTP_NOT_FOUND;

			return $this->response( $data );
		}

		// sent subscription email
		$user = $this->current_user;
		$data = array('user' => $user->username);
		$email_template = $this->load->view('email-template/subscription-update.php', $data, TRUE);
		eagle_send_mail( $user->email, 'EagleJoiner: Subscription updated', $email_template );

		$data['message'] 	= "Subscription updated";
		$data['data'] 		= $subscription;
		$data['status']		= API_Controller::HTTP_OK;

		return $this->response( $data );
	}
}