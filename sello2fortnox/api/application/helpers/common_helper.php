<?php
/**
 * Generate token key
 **/
function generate_token( $string ) {
    $intermediateSalt = md5(uniqid(rand(), true));
    $salt = substr($intermediateSalt, 0, 6);
    return hash("sha256", $string . $salt);
}
/**
 * generate unique string
 *
 **/
function get_unique_string($string = null){
	if( $string == null ){
		$string = uniqid(rand());
	}

	$intermediateSalt = md5(uniqid(rand(), true));
    $salt = substr($intermediateSalt, 0, 6);
    return hash("sha256", $string . $salt);        
}



function strToHex($string){
    $hex = '';
    for ($i=0; $i<strlen($string); $i++){
        $ord = ord($string[$i]);
        $hexCode = dechex($ord);
        $hex .= substr('0'.$hexCode, -2);
    }
    return strToUpper($hex);
}

function generate_random_id( $random_id_length = 2 ) {
	//echo mt_rand();
	//set the random id length 


	//generate a random id encrypt it and store it in $rnd_id 
	$rnd_id = crypt(uniqid(rand(),1)); 

	//to remove any slashes that might have come 
	$rnd_id = strip_tags(stripslashes($rnd_id)); 

	//Removing any . or / and reversing the string 
	$rnd_id = str_replace(".","",$rnd_id); 
	$rnd_id = strrev(str_replace("/","",$rnd_id)); 

	//finally I take the first 10 characters from the $rnd_id 
	$rnd_id = strToHex(substr($rnd_id,0,$random_id_length) ); 

	return $rnd_id;
}


function eagle_send_mail( $email_to, $subject, $message ){
	$ci = & get_instance();
	$ci->load->library('email', $ci->config->item('smtp_config'));

	$ci->email->set_newline("\r\n");

	$ci->email->from($ci->config->item('email_from'), $ci->config->item("email_from_title"));
	
	$ci->email->to( $email_to );

	$ci->email->subject( $subject );

	$ci->email->message( $message );

	return $ci->email->send();
}


function base64_to_file($base64_string, $output_file) {
	$data 		= explode(',', $base64_string);

	$imgdata = base64_decode($data[1]);
	
	$f = finfo_open();

	$mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);

	$split = explode( '/', $mime_type );
	$type = $split[1];

	$output_file = $output_file. ".{$type}";
	
	$ifp 		= fopen($output_file, "w"); 
	
	fwrite( $ifp, $imgdata );
	fclose($ifp); 

	return $type; 
}