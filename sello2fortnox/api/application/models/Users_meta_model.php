<?php
class Users_meta_model extends CI_Model {

    /**
     * Users_meta_model constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $user_id
     * @param bool $meta_key
     * @return mixed
     */
    public function get_user_meta($user_id, $meta_key = FALSE)
    {
        if ($meta_key === FALSE)
        {
            $query = $this->db->get_where('users_meta', array('user_id' => $user_id));
            return $query->result_array();
        }

        $query = $this->db->get_where('users_meta', array('user_id' => $user_id, 'meta_key' => $meta_key));
        return $query->row_array()['meta_value'];
    }

    /**
     * @param $user_id
     * @param $meta_key
     * @param null $meta_value
     * @return mixed
     */
    public function update_user_meta($user_id, $meta_key, $meta_value=null ) {

        $data = array(
            'meta_key' => $meta_key,
            'meta_value' => $meta_value
        );

        if( $this->get_user_meta($user_id, $meta_key) ) {
            $this->db->where('user_id', $user_id);
            $this->db->where('meta_key', $meta_key);

            // echo $sql = $this->db->last_query();
            $update = $this->db->update('users_meta', $data);
            return $update;
        }

        $data['user_id'] = $user_id;
        $update = $this->insert_user_meta($data);
        
        return $update;
        
    }

    public function delete_user_meta($user_id, $meta_key) {
        return $this->db->delete('users_meta', array('user_id'=>$user_id, 'meta_key' => $meta_key));
    }

    public function update_user_meta_data( $user_id , $data ) {

        if( empty($data) ) return;
        // print_r($data); exit;
        return $this->db->update_batch('users_meta', $data, 'user_id');
    }

    public function insert_user_meta( $data ){
       $this->db->insert('users_meta', $data);
       return $this->db->insert_id();
    }

    public function get_user_token( $user_id ){
        $query = $this->db->get_where('users_meta', array('user_id' => $user_id, 'meta_key' =>'auth_token'));
        if($query->num_rows() > 0) {
            return $query->row()->meta_value;
        }

        return false;
    }

    public function get_user_by_token($token) {
        $query = $this->db->get_where('users_meta', array('meta_key' =>'auth_token', 'meta_value' => $token));
        if($query->num_rows() > 0) {
            $query = $this->db->get_where('users', array('id' =>$query->row()->user_id));
            return $query->row();
        }

        return false;
    }


    public function get_user( $args ){
        $query = $this->db->get_where('users_meta', $args);
        $user = $query->row();

        if($query->num_rows() > 0) {
            
            $query = $this->db->get_where('users', array('id' =>$user->user_id));
            $user = $query->row();
            
            if( $user ) {
                $user_meta = $this->get_user_meta($user->id);
                
                $meta_data = array();

                foreach($user_meta as $meta) {
                    $meta_data[$meta['meta_key']] = $meta['meta_value'];
                    $user->meta_data = (object) $meta_data;
                }

                
            }

            return $user;
        }

        return false;   
    }
}