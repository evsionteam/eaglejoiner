<?php
class Activity_model extends CI_Model {

    /**
     * Users_meta_model constructor.
     */
    protected $table;
    public function __construct(){
    	$this->table = "activity";
    }

    public function get( $id = FALSE ) {
    	if ($id === FALSE) {
            $this->db->order_by( 'id', 'DESC' );

            $query = $this->db->get($this->table);

            return $query->result_array();
        }
        $query = $this->db->get_where($this->table, array('id' => $id));
        return $query->row();
    }

    public function get_by_user( $user_id ){
        if( !$user_id ) {
            return false;
        }

        $this->db->where( "user_id", $user_id );
        
        $query = $this->db->get($this->table);

        return $query->result_array();
    }	

    public function insert($data){
    	if( !is_array($data)) return false;
    	
    	if( $this->db->insert($this->table, $data)) {
            return $insert_id = $this->db->insert_id();
        }

        return false;
    }

    public function filter( $args = array(), $limit=20, $start=0 ) {
        // $query = $this->db->get_where($this->table, $args);
        // return $query->result_array();
        $this->db->order_by( 'id', 'DESC' );
        
        $this->db->limit( $limit, $start );
        
        $query = $this->db->get_where($this->table, $args);
        
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }

        return false;
    }


    public function delete( $user_id, $ids ){
        $this->db->where( "user_id", $user_id );

        if( is_array( $ids ) ){
            $this->db->where_in( 'id', $ids );
            return $this->db->delete( $this->table );    
        }
        
        return $this->db->delete( $this->table );
    }


    public function truncate(){
        return $this->db->truncate( $this->table );
    }

    public function record_count( $user_id=null, $args = array() ) {

        if( $user_id == null ){
            return $this->db->count_all( $this->table );
        }

        $this->db->where( 'user_id' , $user_id );
        
        if( count( $args ) > 0 ) {
            $query = $this->db->get_where($this->table, $args);
        }else{
            $query = $this->db->get($this->table);
        }

        return $query->num_rows( );
    }


    public function fetch($user_id = FALSE, $limit=20, $start=0 ) {
        $this->db->limit( $limit, $start );
        $this->db->order_by( 'id', 'DESC' );
        if( $user_id ){
            $query = $this->db->get_where( $this->table, array('user_id'=> $user_id ));
        }else {
            $query = $this->db->get( $this->table );
        }

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }

        return false;
   }

   public function distinct_field( $where , $field = 'activity_type' ) {
        $this->db->distinct();
        $this->db->where( $where );
        $this->db->select( $field );
        $query = $this->db->get( $this->table );
        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $key => $value) {
                $result[] = $value[$field];
            }
        }

        return $result;
    }
	    
}