<?php
class Orders_model extends CI_Model {

    /**
     * Users_meta_model constructor.
     */
    protected $table;
    public function __construct(){
        $this->table = "orders";
    }

    public function get( $id = FALSE ) {
        if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        $query = $this->db->get_where($this->table, array('id' => $id));
        return $query->row();
    }   

    public function insert($data){
        if( !is_array($data)) return false;
        
        if( $this->db->insert($this->table, $data)) {
            return $insert_id = $this->db->insert_id();
        }

        return false;
    }

    public function create( $data ) {
        return $this->insert( $data );
    }

    public function get_row( $user_id, $args){
        $params = array('user_id' => $user_id);
        
        if( isset( $args ) && is_array( $args ) ) {           
            $params = array_merge($params, $args);
        }

        $query = $this->db->get_where($this->table, $params);
        return $query->row();
    }

    public function get_fortnox_order_id($user_id, $args ) {

        $row = $this->get_row($user_id, $args );

        if( $row ) {
            return $row->fortnox_order_id;
        }

        return false;
    }

    public function get_order_row( $user_id, $args ){
        return $this->get_row($user_id, $args );
    }

    public function update_by_sello_id( $sello_id, $args ) {
        $this->db->where('sello_order_id', $sello_id);
        return $this->db->update( $this->table, $args);
    }

    public function update( $id, $args ) {
        $this->db->where( 'id', $id );
        return $this->db->update( $this->table, $args );
    }

    public function get_sello_order_status( $user_id, $args ){
        $row = $this->get_row($user_id, $args );
        if( $row ) {
            return $row->sello_order_status;
        }
        return false;
    }

    public function delete(){

    }

    public function clear(){

    }
        
}