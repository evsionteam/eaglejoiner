<?php
class Stripe_model extends CI_Model {

    /**
     * Stripe_model constructor.
     */
    protected $table;
    public function __construct(){
    	$this->table = "stripe";
    }

    public function get( $id = FALSE ) {
    	if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        $query = $this->db->get_where($this->table, array('id' => $id));
        return $query->row();
    }

    public function get_row_by_user( $user_id = FALSE ) {
        $query = $this->db->get_where($this->table, array('user_id' => $user_id));
        return $query->row();
    }


    public function getByUser( $id = FALSE ) {
        if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        $query = $this->db->get_where($this->table, array('user_id' => $id));
        return $query->row();
    }


    public function insert($data){
    	if( !is_array($data)) return false;
    	
    	if( $this->db->insert($this->table, $data)) {
            return $insert_id = $this->db->insert_id();
        }

        return false;
    }

    public function get_stripe_info( $user_id ) {
        $this->db->select('stripe_id, subscription_id, subscription_id, paid, subscription_canceled_at, subscription_current_period_end, subscription_cancel_at_period_end, subscription_deleted');
        
        $this->db->where('user_id', $user_id );
        
        $query = $this->db->get( $this->table );

        return $query->row();
    }

    public function update( $customer_id, $args ){
        foreach( $args as $key=>$val ) {
            $this->db->set($key, $val);
        }

        $this->db->where('stripe_id', $customer_id);
        
        return $this->db->update( $this->table );
    }

    public function filter( $args ) {
        $query = $this->db->get_where($this->table, $args);
        return $query->result_array();
    }


    public function delete( $user_id, $ids ){
        $this->db->where( "user_id", $user_id );

        if( is_array( $ids ) ){
            $this->db->where_in( 'id', $ids );
            return $this->db->delete( $this->table );    
        }
        
        $this->db->where('id', $ids);
        return $this->db->delete( $this->table );
    }


    public function truncate(){
        return $this->db->truncate( $this->table );
    }

    public function record_count() {
        return $this->db->count_all( $this->table );
    }


    public function fetch($user_id = FALSE, $limit=20, $start=0 ) {
        $this->db->limit( $limit, $start );
        if( $user_id ){
            $query = $this->db->get_where( $this->table, array('user_id'=> $user_id ));
        }else {
            $query = $this->db->get( $this->table );
        }

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }

        return false;
   }
}