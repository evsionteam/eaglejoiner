<?php
class Users_model extends CI_Model {

    public function __construct() {
    }

    public function get_users($id = FALSE) {
        if ($id === FALSE) {
            $query = $this->db->get('users');
            return $query->result_array();
        }

        $query = $this->db->get_where('users', array('id' => $id));
        return $query->row();
    }

    public function get_user_by_email($email){
        $query = $this->db->get_where('users', array('email' => $email));
        return $query->row();
    }

    public function get_user_by_username($username){
        $query = $this->db->get_where('users', array('username' => $username));
        return $query->row();
    }

    public function set_users( $args ) {
        $this->load->library('encrypt');
        $password = $args->password;
        $encrypted_password = $this->encrypt->encode($password);
        $data = array(
            'username' => $args->username,
            'password' => $encrypted_password,
            'email'      => $args->email
        );

        if( $this->db->insert('users', $data)) {
            return $insert_id = $this->db->insert_id();
        }else{
            return 0;
        }
    }


    public function update_user( $user_id, $args ){

        foreach( $args->data as $key=>$val ) {
            $this->db->set($key, $val);
        }

        $this->db->where('id', $user_id);
        
        return $this->db->update('users');
    }

}