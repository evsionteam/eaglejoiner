<?php
class Invoices_model extends CI_Model {

    /**
     * Users_meta_model constructor.
     */
    protected $table;
    public function __construct(){
        $this->table = "invoices";
    }

    public function get( $id = FALSE ) {
        if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        $query = $this->db->get_where($this->table, array('id' => $id));
        return $query->row();
    }   

    public function insert( $data ){
        if( !is_array($data)) return false;
        
        if( $this->db->insert($this->table, $data)) {
            return $insert_id = $this->db->insert_id();
        }

        return false;
    }

    public function create( $data ) {
        return $this->insert( $data );
    }

    public function get_row( $params){
        $query = $this->db->get_where($this->table, $params);
        return $query->row();
    }

}