<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Rest Controller
 * A fully RESTful server implementation for CodeIgniter using one library, one config file and one controller.
 *
 * @package         CodeIgniter
 * @subpackage      Libraries
 * @category        Libraries
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 * @version         3.0.0
 */
 
 class API_Controller extends CI_Controller {

    // Note: Only the widely used HTTP status codes are documented

    // Informational

    const HTTP_CONTINUE = 100;
    const HTTP_SWITCHING_PROTOCOLS = 101;
    const HTTP_PROCESSING = 102;            // RFC2518

    // Success

    /**
     * The request has succeeded
     */
    const HTTP_OK = 200;

    /**
     * The server successfully created a new resource
     */
    const HTTP_CREATED = 201;
    const HTTP_ACCEPTED = 202;
    const HTTP_NON_AUTHORITATIVE_INFORMATION = 203;

    /**
     * The server successfully processed the request, though no content is returned
     */
    const HTTP_NO_CONTENT = 204;
    const HTTP_RESET_CONTENT = 205;
    const HTTP_PARTIAL_CONTENT = 206;
    const HTTP_MULTI_STATUS = 207;          // RFC4918
    const HTTP_ALREADY_REPORTED = 208;      // RFC5842
    const HTTP_IM_USED = 226;               // RFC3229

    // Redirection

    const HTTP_MULTIPLE_CHOICES = 300;
    const HTTP_MOVED_PERMANENTLY = 301;
    const HTTP_FOUND = 302;
    const HTTP_SEE_OTHER = 303;

    /**
     * The resource has not been modified since the last request
     */
    const HTTP_NOT_MODIFIED = 304;
    const HTTP_USE_PROXY = 305;
    const HTTP_RESERVED = 306;
    const HTTP_TEMPORARY_REDIRECT = 307;
    const HTTP_PERMANENTLY_REDIRECT = 308;  // RFC7238

    // Client Error

    /**
     * The request cannot be fulfilled due to multiple errors
     */
    const HTTP_BAD_REQUEST = 400;

    /**
     * The user is unauthorized to access the requested resource
     */
    const HTTP_UNAUTHORIZED = 401;
    const HTTP_PAYMENT_REQUIRED = 402;

    /**
     * The requested resource is unavailable at this present time
     */
    const HTTP_FORBIDDEN = 403;
    // const HTTP_FORBIDDEN = 200;

    /**
     * The requested resource could not be found
     *
     * Note: This is sometimes used to mask if there was an UNAUTHORIZED (401) or
     * FORBIDDEN (403) error, for security reasons
     */
    const HTTP_NOT_FOUND = 404;

    /**
     * The request method is not supported by the following resource
     */
    const HTTP_METHOD_NOT_ALLOWED = 405;

    /**
     * The request was not acceptable
     */
    const HTTP_NOT_ACCEPTABLE = 406;
    const HTTP_PROXY_AUTHENTICATION_REQUIRED = 407;
    const HTTP_REQUEST_TIMEOUT = 408;

    /**
     * The request could not be completed due to a conflict with the current state
     * of the resource
     */
    const HTTP_CONFLICT = 409;
    const HTTP_GONE = 410;
    const HTTP_LENGTH_REQUIRED = 411;
    const HTTP_PRECONDITION_FAILED = 412;
    const HTTP_REQUEST_ENTITY_TOO_LARGE = 413;
    const HTTP_REQUEST_URI_TOO_LONG = 414;
    const HTTP_UNSUPPORTED_MEDIA_TYPE = 415;
    const HTTP_REQUESTED_RANGE_NOT_SATISFIABLE = 416;
    const HTTP_EXPECTATION_FAILED = 417;
    const HTTP_I_AM_A_TEAPOT = 418;                                               // RFC2324
    const HTTP_UNPROCESSABLE_ENTITY = 422;                                        // RFC4918
    const HTTP_LOCKED = 423;                                                      // RFC4918
    const HTTP_FAILED_DEPENDENCY = 424;                                           // RFC4918
    const HTTP_RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL = 425;   // RFC2817
    const HTTP_UPGRADE_REQUIRED = 426;                                            // RFC2817
    const HTTP_PRECONDITION_REQUIRED = 428;                                       // RFC6585
    const HTTP_TOO_MANY_REQUESTS = 429;                                           // RFC6585
    const HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE = 431;                             // RFC6585

    // Server Error

    /**
     * The server encountered an unexpected error
     *
     * Note: This is a generic error message when no specific message
     * is suitable
     */
    const HTTP_INTERNAL_SERVER_ERROR = 500;

    /**
     * The server does not recognise the request method
     */
    const HTTP_NOT_IMPLEMENTED = 501;
    const HTTP_BAD_GATEWAY = 502;
    const HTTP_SERVICE_UNAVAILABLE = 503;
    const HTTP_GATEWAY_TIMEOUT = 504;
    const HTTP_VERSION_NOT_SUPPORTED = 505;
    const HTTP_VARIANT_ALSO_NEGOTIATES_EXPERIMENTAL = 506;                        // RFC2295
    const HTTP_INSUFFICIENT_STORAGE = 507;                                        // RFC4918
    const HTTP_LOOP_DETECTED = 508;                                               // RFC5842
    const HTTP_NOT_EXTENDED = 510;                                                // RFC2774
    const HTTP_NETWORK_AUTHENTICATION_REQUIRED = 511;

    const MESSAGE_TOKEN_INVALID = 'Invalid API key %s'; // %s is the REST API key
    const MESSAGE_TOKEN_NOT_FOUND = 'API key not found!'; // %s is the REST API key
    const MESSAGE_TOKEN_INVALID_CREDENTIAL = 'Invalid credentials';
    const MESSAGE_TOKEN_UNAUTHORIZED = 'This API key does not have access to the requested controller';
    const MESSAGE_AJAX_ONLY = 'Only AJAX requests are allowed';
    const MESSAGE_TOKEN_PERMISSIONS = 'This API key does not have enough permissions';
    const MESSAGE_INVALID_REQUEST = "Invalid request!";
    const MESSAGE_INVALID_DATAFORMAT = "Invalid input formats!";
    const MESSAGE_INVALID_USER = "Invalid API token";
    /**
     * Constructor for the REST API
     *
     * @access public
     * @param string $config Configuration filename minus the file extension
     * e.g: my_rest.php is passed as 'my_rest'
     * @return void
     */
    protected $headers; 
    public $is_valid_user = false;
    public $current_user = false;
    public $is_angular_request = false;

    public $valid_input_keys = array('email', 'password', 'username', 'meta', 'new_password', 'reset_token');

    public function __construct($config = 'rest')
    {
        parent::__construct();

        // $this->headers = $this->input->request_headers();
        foreach ($this->input->request_headers() as $key => $value) {
            $this->headers[trim($key)] = $value;
        }
        
        // echo $this->headers['angular'];
        if( in_array('angular', array_keys($this->headers) ) && $this->headers['angular'] == true){
            $this->is_angular_request = true;
        }
    }

    public function validate() {
        $this->load->model('users_meta_model');
        $token_key = $this->config->item('REST_TOKEN_KEY');
        $token_key = $token_key ? $token_key : "authToken";
        
        if( in_array($token_key, array_keys($this->headers) )) {
            $token = $this->headers[$token_key];
            $user = $this->users_meta_model->get_user_by_token($token);
            if( !$user ) {
                $data['message'] = API_Controller::MESSAGE_TOKEN_INVALID;
                return $this->response($data, API_Controller::HTTP_UNAUTHORIZED );
            }else{
                $this->is_valid_user = true;
                $user->auth_token = $token;

                $user_meta = $this->users_meta_model->get_user_meta($user->id);                
                $meta_data = array();

                foreach($user_meta as $meta) {
                    $meta_data[$meta['meta_key']] = $meta['meta_value'];
                    $user->meta_data = (object) $meta_data;
                }

                $this->current_user = $user;
            }
        }else{
            return $this->response(["message"=>API_Controller::MESSAGE_TOKEN_NOT_FOUND], API_Controller::HTTP_UNAUTHORIZED );
        }

        $this->is_valid_user = true;

    }


    public function response( $data, $status_code = 200 ){
        $data = json_encode( $data );
        if( $this->is_angular_request ){
            $data = ")]}',\n". $data;
        }
        
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header( $status_code )
            ->set_output($data);

    }

     public function invalid_response( $status_code = 200 ){
        $data['error']  = API_Controller::MESSAGE_INVALID_USER;
        $data['status'] = API_Controller::HTTP_FORBIDDEN;

        $data = json_encode( $data );
        if( $this->is_angular_request ){
            $data = ")]}',\n". $data;
        }
        
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header( $status_code )
            ->set_output($data);
     }

    public function error_response( $obj, $status_code = 200 ){

    }

    public function validate_inputs( $data ) {
        $valid = true;
        foreach ( array_keys( $data ) as $key ):
            if ( !in_array( $key, $this->valid_input_keys ) ):
                $valid = false;
            endif;
        endforeach;

        foreach( $data as $key=>$val ){
            if( empty( $val ) ){
                $valid = false;
            }
        }

        return $valid;
    }

}