
// notification 
(function ($) {

    function notification(param) {

        this.animation = param.animation,
                this.toggler = param.toggler,
                this.exceptions = param.exceptions;

        this.init = function () {
            var that = this;
            
            // for stop propagation 
            var stopToggler = this.implode(this.exceptions);
            if (typeof stopToggler !== 'undefined') {
                $(document).on('click', stopToggler, function (e) {
                    e.stopPropagation();
                });
            }

            // for toggle class
            var toggler = this.implode(this.toggler);
            if (typeof toggler !== 'undefined') {

                $(document).on('click touchstart', toggler, function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                    that.toggle();
                });
            }
        }

        // open class toggler
        this.toggle = function () {
            var selectors = this.implode(this.animation);
            if (typeof selectors !== 'undefined') {
                $(selectors).toggleClass('open');
            }
        }

        // array selector maker
        this.implode = function (arr, imploder) {

            // checking arg is array or not
            if (!(arr instanceof Array)) {
                return arr;
            }
            // setting default imploder
            if (typeof imploder == 'undefined') {
                imploder = ',';
            }

            // making selector
            var data = arr;
            var ele = '';
            for (var j = 0; j < arr.length; j++) {
                ele += arr[j];
                if (j !== arr.length - 1) {
                    ele += imploder;
                }
            }
            data = ele;
            return data;
        }
    }

    var notificationConfig = {
        animation: '.notification-overlay',
        exceptions: ['.notification-board'],
        toggler: ['#close', '.notification-overlay', '#notification-icon']
    };
    new notification(notificationConfig).init();

    var menuConfig = {
        toggler: '.menu-icon',
        animation: ['.side-menu', '.menu-icon', '.main']
    }
    new notification(menuConfig).init();

    var searchConfig = {
        animation: ['.title-n-menu', '.user-options .user-name', '.main-overlay',  '.user-options .profile-pic', '.search-ani', '.search', '.notification', '.logout'],
        toggler: ['.search-icon',  '.search-ani .fa-close', '.main-overlay']
    }
    new notification(searchConfig).init();
    
})(jQuery)

//
//+ (function ($) {
//    function search(param) {
//
//        this.icon = param.icon,
//                this.selector = param.selector,
//                that = this;
//
//        this.init = function () {
//            $(document).on('click touchstart', this.icon, function (e) {
//                e.stopPropagation();
//                that.selectPre();
//                that.toggle();
//            });
//        }
//
//        this.toggle = function () {
//            $(that.selector).toggleClass('open');
//        }
//
//        this.selectPre = function () {
//            $(this.selector).prev().toggle(1, 'linear');
//        }
//
//    }
//
//    var searchConfig = {
//        icon: '.search .search-icon ',
//        selector: '.search '
//    }
//    new search(searchConfig).init();
//
//})(jQuery)

