$(document).ready(function () {

    // tooltip
    $('html.no-touch [data-toggle="tooltip"]').tooltip();

    //custome select box
    if ($('#filer-icon').length > 0) {
        $('#response-type').selectmenu();
        $('#action-type').selectmenu();
        $('#datepicker-from').datepicker();
        $('#datepicker-to').datepicker();
    }

    // Nice scroll
    $('html').niceScroll({
        cursorcolor: "#1E90B3",
        background: "#ccc",
         cursoropacitymin: 0.4,
         cursorwidth: "6px",
          cursorborder: "none",
          cursorborderradius: "0"
    });
    
    // focus search input
    $(document).on('click', '.search .search-icon', function(){
       $('.search-ani input').focus(); 
    });
    
    // filter 
    $(document).on('click', '#filer-icon', function () {
        $('.filter-options').slideToggle(180);
    });

});