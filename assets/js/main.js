

(function ($) {


    /* ========================================================
     * DESKTOP ANIMATION
     * ======================================================== */
    function animation(arg) {

        var param = {
            line: '.line',
            joinUs: '.join-us',
            firstPlug: '.first-plug',
            secondPlug: '.second-plug',
            selloShopCircle: '.sello-shop-circle',
            traderaCircle: '.tradera-circle',
            fyndiqCircle: '.fyndiq-circle',
            cdonCircle: '.cdon-circle',
            amazonWithCircle: '.amazon-with-circle',
            pestaShopCircle: '.pesta-shop-circle',
            wooCommerceCircle: '.woo-commerce-circle',
            sello: '.sello',
            fortnoxLogo: '.fortnox-logo',
            logo: '.logo',
            fortnoxIcon: '.fortnox-logo',
            fIconPart: '.f-icon-part',
            rectBg: '.rect-bg',
            connectorLineOne: '#connector-line-one',
            connetorLineTwo: '#connector-line-two',
            selloConnect: '.sello-connect',
            eagleConnect: '.eagle-connect',
            joinUs: '.join-us',
                    selectAll: 'body *',
            loader: '.loader',
            fortnoxConnect: '.fortnox-connect'
        }

        //extend param
        $.extend(param, arg);

        // variable
        this.line = param.line,
                this.joinUs = param.joinUs,
                this.firstPlug = param.firstPlug,
                this.secondPlug = param.secondPlug,
                this.selloShopCircle = param.selloShopCircle,
                this.traderaCircle = param.traderaCircle,
                this.fyndiqCircle = param.fyndiqCircle,
                this.cdonCircle = param.cdonCircle,
                this.amazonWithCircle = param.amazonWithCircle,
                this.pestaShopCircle = param.pestaShopCircle,
                this.wooCommerceCircle = param.wooCommerceCircle,
                this.sello = param.sello,
                this.fortnoxLogo = param.fortnoxLogo,
                this.logo = param.logo,
                this.fortnoxIcon = param.fortnoxIcon,
                this.rectBg = param.rectBg,
                this.fIconPart = param.fIconPart,
                this.selloConnect = param.selloConnect,
                this.connetorLineOne = param.connectorLineOne,
                this.connetorLineTwo = param.connetorLineTwo,
                this.eagleConnect = param.eagleConnect,
                this.fortnoxConnect = param.fortnoxConnect,
                this.joinUs = param.joinUs,
                this.selectAll = param.selectAll,
                this.loader = param.loader,
                that = this;

        var accounts = [that.cdonCircle, that.pestaShopCircle, that.amazonWithCircle, that.wooCommerceCircle, that.fyndiqCircle, that.traderaCircle, that.selloShopCircle]


        // constructor function
        this.init = function () {
            // logo animation
            this.Animation();
        }

        //logo with background
        this.Animation = function () {
            var tl = new TimelineLite();

            tl
                    .to($(this.selectAll), 0, {
                        visibility: 'visible'
                    })
                    .to($(this.loader), 1, {
                        scale: 2,
                        opacity: 0,
                        ease: Back.easeInOut.config(4),
                        onComplete: this.removeLoader
                    })
                    .to($(this.line), 1.5, {
                        scaleX: .3,
                        x: '3%',
                        ease: Elastic.easeOut.config(1, 0.4),
                        scaleY: 2,
                        skewX: -30
                    }, '-=0.3')
                    .from($(this.logo), 1, {
                        scale: 3,
                        opacity: 0,
                        transformOrigin: 'center',
                        ease: Back.easeInOut.config(4),
                        onComplete: this.complete
                    }, '-=1')
                    .from($(this.sello), 0.5, {
                        scale: 0,
                        opacity: 0,
                        transformOrigin: 'center',
                        ease: Back.easeOut.config(3),
                        onComplete: this.selloAccounts
                    })
                    .staggerFromTo(accounts, 0.4, {
                        x: 100,
                        y: -30,
                        scale: 0,
                        opacity: 0
                    }, {
                        x: 0,
                        y: 0,
                        scale: 1,
                        opacity: 1
                    }, 0.13)
                    .from($(this.firstPlug), 1, {
                        transformOrigin: 'left',
                        rotationY: -20,
                        delay: 3,
                        ease: SlowMo.ease.config(0.5, 0.6, false),
                        scale: 0,
                        delay:0.5,
                                opacity: 0,
                        onComplete: this.shakeAni
                    })
                    .from($(this.fortnoxLogo), 0.5, {
                        scale: 0,
                        opacity: 0,
                        rotation: 60,
                        delay: 0.3,
                        transformOrigin: 'center',
                        ease: Back.easeOut.config(1.7)
                    })
                    .from($(this.secondPlug), 1, {
                        transformOrigin: 'right',
                        rotation: -20,
                        scale: 0,
                        ease: SlowMo.ease.config(0.5, 0.6, false),
                        opacity: 0,
                        onComplete: this.fortnoxIconAnimation,
                        onComplete      : this.shakeAni
                    })
                    .from($(this.rectBg), 0.5, {
                        scale: 0,
                        opacity: 0,
                        delay: 0.3,
                        transformOrigin: 'center',
                        ease: Back.easeOut.config(1.7),
                        onComplete: this.fortnoxIconAnimation

                    })
                    .staggerFromTo($(that.fIconPart), 0.2, {
                        transformOrigin: 'center',
                        opacity: 0,
                        visibility: 'visible',
                        scaleX: 0
                    }, {
                        opacity: 1,
                        scaleX: 1
                    }, 0.05)
                    .to($(this.selloConnect), 0.8, {
                        scale: 1.1,
                        transformOrigin: 'center',
                        ease: Back.easeOut.config(2.5),
                        onComplete: this.connectorLineOne

                    }, '+=0.5')
                    .to($(this.eagleConnect), 0.8, {
                        scale: 1.1,
                        transformOrigin: 'center',
                        delay: 0.8,
                        ease: Back.easeOut.config(2.5),
                        onComplete: this.connectorLineTwo

                    })
                    .to($(this.fortnoxConnect), 0.8, {
                        scale: 1.1,
                        transformOrigin: 'center',
                        delay: 0.8,
                        ease: Back.easeOut.config(2.5)
                    })
                    .to($(this.fortnoxConnect + ', ' + this.selloConnect + ', ' + this.eagleConnect), 2, {
                        scale: 5,
                        opacity: 0,
                        delay: 0.5,
                        transformOrigin: 'center',
                        ease: Back.easeOut.config(2.5)
                    })
                    .to($(this.fortnoxConnect), 0.1, {
                        onComplete: this.bounce
                    }, '-=1.5')
        }

        // bounce
        this.bounce = function () {
            $(that.joinUs).attr("class", "join-us bounce");
        }
        // connector line One function
        this.connectorLineOne = function () {
            $(that.connetorLineOne).attr("class", "connect-line d-one");
        }
        // connector line Two function
        this.connectorLineTwo = function () {
            $(that.connetorLineTwo).attr("class", "connect-line d-two");
        }
        // fortnox top icon stagger animation
        this.fortnoxIconAnimation = function () {
            TweenMax
        }
        //remove loader
        this.removeLoader = function () {
            $(that.loader).remove();
        }

        this.shakeAni = function () {
            $(that.logo).attr("class", "logo shake");
            setTimeout(function () {
                $(that.logo).attr("class", "logo");
            }, 1000);
        }
    }


    /* ========================================================
     * MOBILE ANIMATION
     * ======================================================== */
    function animationForMobile(arg) {

        var param = {
            line: '.line',
            loader: '.loader',
            mobile: '.mobile',
            selectAll: 'body *'
        }
        $.extend(param, arg);
        this.line = param.line,
                this.mobile = param.mobile,
                this.selectAll = param.selectAll,
                this.loader = param.loader,
                this.selectAll = param.selectAll;

        this.init = function () {
            this.animation();
        }
        this.animation = function () {
            var tl = new TimelineLite();

            tl
                    .to($(this.selectAll), 0, {
                        visibility: 'visible'
                    })
                    .to($(this.loader), 1, {
                        scale: 2,
                        opacity: 0,
                        ease: Back.easeInOut.config(4),
                        onComplete: this.removeLoader
                    })
                    .to($(this.mobile), 0.5, {
                        visibility: 'visible'
                    })
        }
    }

    /* ========================================================
     * LINE ANIMATION FOR RESPONSIVE
     * ======================================================== */
    function lineAnimation(arg) {
        var param = {
            line: '.line',
            status: true
        }
        var tl = new TimelineLite();
        $.extend(param, arg);
        this.line = param.line
        if (param.status) {
            tl.to($(this.line), 1.5, {
                scaleX: .3,
                x: '3%',
                ease: Elastic.easeOut.config(1, 0.4),
                scaleY: 2,
                skewX: -30
            }, '-=0.3')
        } else {
            tl.to($(this.line), 1.5, {
                scaleX: 1,
                 x: '0%',
                ease: Elastic.easeOut.config(1, 0.4),
                scaleY: 2,
                skewX: 0
            }, '-=0.3')
        }
    }
    
    $(window).resize(function () {
        var windowWidth = $(window).width();
        if (windowWidth > 500) {
            lineAnimation({
                status: true
            })
        } else {
            lineAnimation({
                status: false
            })
        }
    })
    $(window).load(function () {
        var windowWidth = $(window).width();
        if (windowWidth > 500) {
            new animation().init();
        } else {
            new animationForMobile().init();
        }
    });

})(jQuery)



